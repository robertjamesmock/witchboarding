﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstract class that can be extended to trigger arbitrary behavior on a set of beats.
/// </summary>
public abstract class AudioTriggerer : MonoBehaviour {

    /// <summary>
    /// How far ahead the AudioTriggerers schedule events
    /// </summary>
    public const float AUDIO_TRIGGER_LOOKAHEAD = 0.25f;

    #region Abstract members

    /// <summary>
    /// Override to define what behavior gets triggered on the specified beats
    /// </summary>
    public abstract void OnTrigger();

    /// <summary>
    /// Override to implement additional behavior to happen on awake
    /// </summary>
    protected virtual void OnAwake() { }

    /// <summary>
    /// Override to implement additional behavior to happen on start
    /// </summary>
    protected virtual void OnStart() { }

    /// <summary>
    /// Override to implement additional behavior to happen on update
    /// </summary>
    protected virtual void OnUpdate() { }

    #endregion

    #region Public properties

    /// <summary>
    /// The set of note locations that the specified behavior will trigger on.
    /// NoteLocations must be ordered and unique, and must contain no null values.
    /// Changes made after Start() will only take effect after restarting the game.
    /// </summary>
    public NoteLocation[] NoteLocations;

    /// <summary>
    /// The clip that gets played on the specified beats.
    /// Changes made after Start() will only take effect after restarting the game.
    /// </summary>
    public AudioClip clip;

    /// <summary>
    /// If true, this AudioTriggerSubject starts playing on Start().
    /// Changes made after Start() will only take effect after restarting the game.
    /// </summary>
    public bool playOnStart;

    #endregion

    #region Public read-only properties

    /// <summary>
    /// True if this AudioTriggerSubject is currently playing, false otherwise.
    /// </summary>
    public bool Playing { get; private set; }

    /// <summary>
    /// True if this AudioTriggerSubject is going to start at the beginning of the next loop, false otherwise
    /// </summary>
    public bool Starting { get; private set; }

    /// <summary>
    /// True if this AudioTriggerSubject is going to stop at the beginning of the next loop, false otherwise
    /// </summary>
    public bool Stopping { get; private set; }

    /// <summary>
    /// Time in seconds until the next trigger
    /// </summary>
    public double TimeUntilNextTrigger => nextTriggerTime - AudioSettings.dspTime;

    #endregion

    #region Fields

    private int nlIndex;
    private double nextTriggerTime;
    private int myLoopsSoFar;
    private List<NoteLocation> noteLocations;
    private List<AudioSource> audioSources;
    #endregion

    #region Public methods

    /// <summary>
    /// Make this AudioTriggerSubject start playing immediately (even if in the middle of a loop).
    /// </summary>
    public void Play()
    {
        CheckManagerPlaying();

        if (Playing) return;

        Stopping = false;
        Starting = false;
        Playing = true;
        myLoopsSoFar = AudioTriggerManager.Instance.LoopsSoFar;

        // find the index of the next noteLocation that lies outside of the audio trigger lookahead
        // and set the next trigger time accordingly
        nlIndex = 0;
        SetNextTriggerTime();
        while (NextTriggerWithinLookAhead())
            //while (NextTriggerInPast())
        {
            nlIndex++;
            if (nlIndex == noteLocations.Count)
            {
                myLoopsSoFar++;
                nlIndex = 0;
            }

            SetNextTriggerTime();
        }
    }

    /// <summary>
    /// Make this AudioTriggerSubject start playing at the start of the next loop.
    /// </summary>
    /// <exception cref="System.InvalidOperationException">Thrown when the AudioTriggerManager is not currently playing</exception>
    public void PlayOnNextLoop()
    {
        CheckManagerPlaying();

        if (Playing || Starting) return;

        Stopping = false;
        Starting = true;
        nlIndex = 0;
        myLoopsSoFar = AudioTriggerManager.Instance.LoopsSoFar + 1;  // pretend we've completed the current loop, so that we start on the next loop
        SetNextTriggerTime();
    }

    /// <summary>
    /// Make this AudioTriggerSubject stop playing immediately (even if in the middle of a loop).
    /// </summary>
    /// <exception cref="System.InvalidOperationException">Thrown when the AudioTriggerManager is not currently playing</exception>
    public void Stop()
    {
        CheckManagerPlaying();

        Playing = false;
        Starting = false;
        Stopping = false;
    }

    /// <summary>
    /// Make this AudioTriggerSubject stop playing at the start of the next loop.
    /// </summary>
    /// <exception cref="System.InvalidOperationException">Thrown when the AudioTriggerManager is not currently playing</exception>
    public void StopOnNextLoop()
    {
        CheckManagerPlaying();

        if (Stopping) return;

        Starting = false;
        Stopping = true;
    }

    #endregion

    #region Monobehavior methods

    private void Awake()
    {
        ValidateNoteLocations();

        // initialize
        noteLocations = new List<NoteLocation>(NoteLocations);
        audioSources = new List<AudioSource>();

        OnAwake();
    }

    private void OnEnable()
    {
        nlIndex = -1; // indicates this AudioTriggerSubject has never played
        Playing = false;
        Starting = false;
        Stopping = false;
    }

    private void Start()
    {
        // create a new AudioSource for each NoteLocation, and save them in audioSources
        for (int i = 0; i < noteLocations.Count; i++)
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            audioSource.clip = clip;
            audioSource.spatialBlend = 1;
            audioSource.dopplerLevel = 0;
            audioSources.Add(audioSource);
        }

        Debug.Assert(noteLocations.Count == audioSources.Count);

        if (playOnStart) PlayOnNextLoop();

        OnStart();
    }

    private void Update()
    {
        if (!AudioTriggerManager.Instance.Playing || !(Playing || Starting))
        {
            OnUpdate();
            return;
        }

        while (NextTriggerWithinLookAhead())
        {
            if (Starting)
            {
                Playing = true;
                Starting = false;
            }

            // schedule audio
            audioSources[nlIndex].clip = clip;
            audioSources[nlIndex].PlayScheduled(nextTriggerTime);

            // schedule OnTrigger()
            Invoke("OnTrigger", (float)(nextTriggerTime - AudioSettings.dspTime));

            nlIndex++;
            if (nlIndex == noteLocations.Count)
            {
                if (Stopping)
                {
                    Playing = false;
                    Stopping = false;
                    return;
                }
                myLoopsSoFar++;
                nlIndex = 0;
            }

            SetNextTriggerTime();
        }

        OnUpdate();
    }

    #endregion

    #region Private helpers

    private void SetNextTriggerTime()
    {
        Debug.Assert(nlIndex >= 0 && nlIndex < noteLocations.Count);
        
        nextTriggerTime = AudioTriggerManager.Instance.DspTimeInLoopFromNoteLocation(noteLocations[nlIndex])
            + myLoopsSoFar * AudioTriggerManager.Instance.LoopLengthSeconds
            + AudioTriggerManager.Instance.StartDspTime;
    }

    //private bool NextTriggerInPast()
    //{
    //    return nextTriggerTime < AudioSettings.dspTime;
    //}

    private bool NextTriggerWithinLookAhead()
    {
        return nextTriggerTime < AudioSettings.dspTime + AUDIO_TRIGGER_LOOKAHEAD;
    }

    /// <summary>
    /// Ensures NoteLocations is non-null, non-empty, contains no null values, and is ordered and unique
    /// </summary>
    private void ValidateNoteLocations()
    {
        // check that NoteLocations is non-null and non-empty
        CheckNotNull(NoteLocations, "NoteLocations cannot be null");
        if (NoteLocations.Length == 0) throw new System.InvalidOperationException("NoteLocations cannot be empty");

        // check that NoteLocations is ordered and unique, and contains no null values
        NoteLocation nlPrev = NoteLocations[0];
        CheckNotNull(nlPrev, "NoteLocations cannot contain null values");

        for (int i = 1; i < NoteLocations.Length; i++)
        {
            NoteLocation nl = NoteLocations[i];
            CheckNotNull(nl, "NoteLocations cannot contain null values");
            if (nlPrev >= nl)
            {
                throw new System.InvalidOperationException("NoteLocations must be ordered and unique");
            }
            nlPrev = nl;
        }
    }

    private void CheckManagerPlaying()
    {
        if (!AudioTriggerManager.Instance.Playing) throw new System.InvalidOperationException("The AudioTriggerManager is not currently playing");
    }

    private void CheckNotNull(object obj, string message = "")
    {
        if (obj == null) throw new System.InvalidOperationException(message);
    }

    #endregion
}
