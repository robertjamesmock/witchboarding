﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using System;

public abstract class mono : MonoBehaviour {

    public virtual void SetInput(int index, mono mono) { Debug.LogError("tried to set mono input for " + this.GetType().ToString()+" but that's not implemented!"); }
    //public virtual void SetInput(int index, NoteControl noteControl) { Debug.LogError("tried to set noteControl input for " + this.GetType().ToString() + " but that's not implemented!"); }
    //public virtual void SetInput(int index, Knob knob) { Debug.LogError("tried to set knob input for " + this.GetType().ToString() + " but that's not implemented!"); }
    
    
    public virtual ReadOnlyCollection<mono> monos()
    {
        Debug.LogWarning("empty monos set for " + GetType().ToString());
        return new List<mono>().AsReadOnly();
    }

    //public virtual ReadOnlyCollection<Knob> knobs()
    //{
    //    Debug.LogWarning("empty knobs set for " + GetType().ToString());
    //    return new List<Knob>().AsReadOnly();
    //}

    //public virtual ReadOnlyCollection<NoteControl> noteControls()
    //{
    //    Debug.LogWarning("empty notecontrols set for " + GetType().ToString());
    //    return new List<NoteControl>().AsReadOnly();
    //}

    private void Awake()
    {
        SetLength(1024);
    }

    [HideInInspector]
	public float[] fill = new float[1024];
	private bool[] doneBox = { false };
	protected int length = 1024;

	public float[] gibSignal(List<bool[]> doneBoxes) {
		if (!doneBox[0]) {
			doneBox[0] = true;
			doneBoxes.Add(doneBox);
			getSignal(doneBoxes);
		}
		return fill;
	}

	/// <summary>
	/// build "fill" to length
	/// </summary>
	/// <param name="length"></param>
	/// <returns>A float array </returns>
	abstract protected void getSignal(List<bool[]> doneBoxes);//nobody else should ever call this

	public virtual void SetLength(int length) {
		fill = new float[length];
		this.length = length;
	}

}
