﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bitcrushFilter : MonoBehaviour {
    public float resolution;

    private void OnAudioFilterRead(float[] data, int channels)
    {
        if (resolution < 1)
        {
            throw new System.ArgumentException();
        }

        for(int i = 0; i < data.Length; i++)
        {
            data[i] = Mathf.Floor(data[i] * resolution) / resolution;
        }
    }
}
