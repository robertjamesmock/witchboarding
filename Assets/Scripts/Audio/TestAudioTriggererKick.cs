﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A demo AudioTriggerer
/// </summary>
public class TestAudioTriggererKick : AudioTriggerer {

    // define what behavior triggers on the beats
    public override void OnTrigger()
    {
        transform.RotateAroundLocal(new Vector3(0, 0, 1), 0.4f);
    }
}
