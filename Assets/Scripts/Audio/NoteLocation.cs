﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Type representing a rhythmic location (e.g. beat 3 of bar 2)
/// </summary>
[System.Serializable]
public class NoteLocation : System.IComparable<NoteLocation>
{
    public int BarNum;
    public int BeatNum;
    public double Division;

    private static int NumBeatsInBar => AudioTriggerManager.Instance.NumBeatsInBar;

    /// <summary>
    /// Creates a new note location
    /// </summary>
    /// <param name="barNum">The bar in which the note occurs. (Zero-indexed)</param>
    /// <param name="beatNum">The beat in the bar in which the note occurs. (Zero-indexed)</param>
    /// <param name="division">How far through the beat the note occurs (e.g. 0: right on the beat, 1.0/2.0: the "and"
    /// of the beat, or 2.0/3.0: the third partial of a triplet starting on the beat).</param>
    /// <exception name="IllegalArgumentException">if barNum &lt; 0 or &gt; the number of bars in a loop </exception>
    /// <exception name="IllegalArgumentException">if beatNum &lt; 0 or &gt; the number of beats in a bar </exception>
    /// <exception name="IllegalArgumentException">if divion &lt; 0 or &gt; 1 </exception>
    public NoteLocation(int barNum, int beatNum, double division)
    {
        CheckGreaterThanOrEqualToZero(barNum, "barNum");
        CheckGreaterThanOrEqualToZero(beatNum, "beatNum");

        if (beatNum >= NumBeatsInBar)
        {
            throw new System.ArgumentException("beatNum: " + beatNum + " must be < number of beats in a bar: " + NumBeatsInBar);
        }

        if (division < 0 || division >= 1)
        {
            throw new System.ArgumentException("division must be >= 0 and < 1, but it is: " + division);
        }

        BarNum = barNum;
        BeatNum = beatNum;
        Division = division;
    }

    /// <summary>
    /// This &lt; other if this note occurs before other note, this = other if they occur
    /// at the same time, and this &gt; other if this occurs after other.
    /// </summary>
    public int CompareTo(NoteLocation other)
    {
        if (BarNum != other.BarNum)
        {
            return BarNum - other.BarNum;
        }

        if (BeatNum != other.BeatNum)
        {
            return BeatNum - other.BeatNum;
        }

        return Division.CompareTo(other.Division);
    }

    public static bool operator >(NoteLocation a, NoteLocation b)
    {
        return a.CompareTo(b) > 0;
    }

    public static bool operator <(NoteLocation a, NoteLocation b)
    {
        return a.CompareTo(b) < 0;
    }

    public static bool operator >=(NoteLocation a, NoteLocation b)
    {
        return a.CompareTo(b) >= 0;
    }

    public static bool operator <=(NoteLocation a, NoteLocation b)
    {
        return a.CompareTo(b) <= 0;
    }

    public static NoteLocation operator +(NoteLocation a, NoteLocation b)
    {
        int barNum = a.BarNum + b.BarNum;
        int beatNum = a.BeatNum + b.BeatNum;
        double division = a.Division + b.Division;

        if (division >= 1)
        {
            beatNum++;
            division -= 1;
        }

        if (beatNum >= NumBeatsInBar)
        {
            barNum++;
            beatNum -= NumBeatsInBar;
        }

        return new NoteLocation(barNum, beatNum, division);
    }

    /// <summary>
    /// Throws an exception if x &lt; 0
    /// </summary>
    private void CheckGreaterThanOrEqualToZero(int x, string name = "x")
    {
        if (x < 0)
        {
            throw new System.ArgumentException(name + " is " + x + ", but it must be >= 0");
        }
    }

    public override string ToString()
    {
        return BarNum + "-" + BeatNum + "-" + Division;
    }
}
