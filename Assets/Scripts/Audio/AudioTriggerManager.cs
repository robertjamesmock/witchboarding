﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Keeps track of the audio timeline for the scene, as well as details such as bpm, length of loop, and time signature.
/// </summary>
public class AudioTriggerManager : MonoBehaviour
{
    public float bpm; // tempo in beats per minute
    public int NumBarsInLoop;
    public int NumBeatsInBar;
    public bool Playing { get; private set; }
    public double StartDspTime { get; private set; }  // AudioSettings.dspTime when audio started
    public double LoopLengthSeconds => DspTimeInLoopFromNoteLocation(NumBarsInLoop, 0, 0);
    public double BarLengthSeconds => DspTimeInLoopFromNoteLocation(1, 0, 0);
    public double BeatLengthSeconds => DspTimeInLoopFromNoteLocation(0, 1, 0);
    public double PlayedTime => AudioSettings.dspTime - StartDspTime;  // time passed since audio started
    public int LoopsSoFar => Mathf.FloorToInt((float)(PlayedTime / LoopLengthSeconds));
    public double TimeUntilNextLoop => PlayedTime % LoopLengthSeconds;
    public double TimeUntilNextBar => PlayedTime % BarLengthSeconds;
    public double TimeUntilNextBeat => PlayedTime % BeatLengthSeconds;

    static AudioTriggerManager _instance;

    public static AudioTriggerManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<AudioTriggerManager>();
                if (_instance == null)
                {
                    Debug.Assert(false, "The scene must have an AudioTriggerManager");
                }
            }
            return _instance;
        }
    }

    private void Awake()
    {
        StartDspTime = double.NaN;
        Playing = false;
        StartAudio();
    }

    void StartAudio()
    {
        Debug.Assert(!Playing);

        StartDspTime = AudioSettings.dspTime;
        Playing = true;
    }

    // Seconds from the start of a loop that the passed NoteLocation should occur
    public double DspTimeInLoopFromNoteLocation(NoteLocation nl)
    {
        return DspTimeInLoopFromNoteLocation(nl.BarNum, nl.BeatNum, nl.Division);
    }

    // Seconds from the start of a loop that the passed NoteLocation should occur
    public double DspTimeInLoopFromNoteLocation(int bar, int beat, double division)
    {
        double totalBeats = bar * NumBeatsInBar + beat + division;
        double beatsPerSecond = bpm / 60;
        double totalSeconds = totalBeats / beatsPerSecond;
        return totalSeconds;
    }
}
