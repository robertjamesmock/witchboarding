﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Demo class to demonstrate how to start and stop AudioTriggerSubjects
/// </summary>
public class TestAudioStartStop : MonoBehaviour {
    AudioTriggerer kick;
    AudioTriggerer snare;

    void Start () {
        kick = GetComponent<TestAudioTriggererKick>();
        snare = GetComponent<TestAudioTriggererSnare>();
    }
	
	void Update () {

        // start snare at top of loop
        if (Time.time > 5)
        {
            snare.PlayOnNextLoop();
        }

        // stop kick in middle of loop
        if (Time.time > 10 && Time.time < 15)
        {
            kick.Stop();
        }

        // play kick in middle of loop
        if (Time.time > 15 && Time.time < 17)
        {
            kick.Play();
        }

        // stop kick at top of loop
        if (Time.time > 17)
        {
            kick.StopOnNextLoop();
        }
    }
}
