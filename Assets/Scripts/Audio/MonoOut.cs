﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoOut : MonoBehaviour {

    public mono input;

    private void Awake()
    {
        if (input == null)
        {
            Debug.LogError("No input to the monoout!");
            Debug.Break();
        }
    }

    private void OnAudioFilterRead(float[] data, int channels)
    {
		List<bool[]> doneBoxes = new List<bool[]>() ;
        var datta = input.gibSignal(doneBoxes);
		foreach (var box in doneBoxes) { box[0] = false; }
        for(int i = 0; i < data.Length/channels; i++)
        {
            for (int j = 0; j < channels; j++)
            {
                try
                {
                    data[i * channels + j] = datta[i];
                } catch(System.IndexOutOfRangeException e)
                {
                    Debug.LogError("Your out's input is returning an empty/incorrect-size sample array to gibSignal!");
                    Debug.Break();
                }
            }
        }
    }


}
