﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A demo AudioTriggerer
/// </summary>
public class TestAudioTriggererSnare : AudioTriggerer
{

    // define what behavior triggers on the beats
	public override void OnTrigger()
	{
        transform.localScale += new Vector3(0.2f, 0.2f, 0.2f);
    }
}
