﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatPointer : MonoBehaviour {

	public Transform holder;
	public Transform wrist;
	public swooshyStick stick;

	public float radius;
	public float flickBoundary;
	public Vector2 pointDirection { get { return transform.right; } }

	// Use this for initialization
	void Start () {
        holder.GetComponent<Playboarder>().batPointer = this;
	}

	Vector3 lastMousePos=Vector3.zero;
	float lastMouseUpdate=0f;
	Vector2 lastStickPos = Vector2.zero;
	float lastStickUpdate = 0f;

    // Update is called once per frame
    void Update()
    {
		
		Vector3 mousePos = Input.mousePosition;
		if (mousePos != lastMousePos) {
			lastMousePos = mousePos;
			lastMouseUpdate = Time.time;
		}
		Vector2 stickPos = new Vector2(Input.GetAxis("RSH"), Input.GetAxis("RSV"));
		if (stickPos != lastStickPos) {
			lastStickPos = stickPos;
			lastStickUpdate = Time.time;
		}
		Vector3 offset;
		if (lastMouseUpdate > lastStickUpdate) {
			Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(mousePos);
			mouseWorldPos.z = 0;
			Vector3 distt = (mouseWorldPos - holder.position); // holder.position is at center of rabbz right?
			offset = distt.normalized * Mathf.Min(radius, distt.magnitude);
		} else {
			offset = stickPos * radius;
			if(stickPos.magnitude > flickBoundary & lastStickPos.magnitude < flickBoundary) {
				stick.SwingBat();
				Debug.Log(stickPos.magnitude);
			}
		}

        transform.position = holder.position + offset;
		wrist.localRotation = Quaternion.Euler(0, 90 * (1 - Mathf.Sign(offset.x)), 90 * (1 - Mathf.Sign(offset.x)));
        transform.rotation = Quaternion.FromToRotation(Vector2.right, offset.normalized);
        //Quaternion.Euler(offset.normalized*180f);
    }
}
