﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Playboarder : MonoBehaviour {
    public static Playboarder Instance;

    public float jumpSpeed;
	Rigidbody2D body;
	public float maxSpeed;
	public float iFrameDuration;

	float hypeRate {
		get { return maxSpeed / chillRate - maxSpeed; }
	}
	public float chillRate;
	public int maxJumps = 2;
    public float straightenTime = 0.25f;
    public BatPointer batPointer;
    public swooshyStick stick;
    public int MaxHealth = 3;
    public bool immune = false;

    public int Health { get; private set; }

	int currentJumps;
    float currentStraightenTime;

    enum moveState {
		rollLeft,
		faceLeft,
		faceRight,
		rollRight
	}
	float animationCountdown;
	Vector3 rollingVelocity = Vector2.zero;


	void Awake() {
        currentJumps = 2;
        currentStraightenTime = 0;
		body = GetComponent<Rigidbody2D>();
        body.freezeRotation = true;
        Health = MaxHealth;
        immune = false;
        stick = GetComponentInChildren<swooshyStick>();
        Instance = this;
    }

    // Update is called once per frame
    void Update () {
        //rollingVelocity = (rollingVelocity + Input.GetAxis("Horizontal") * hypeRate * Time.deltaTime * transform.right) * chillRate;
        //Vector3 momentum = body.velocity;

        currentStraightenTime += Time.deltaTime;
        if (currentStraightenTime > straightenTime)
        {
            transform.up = Vector3.up;
            //return;
        }

        Vector3 xvel = Vector3.Project((Vector3)body.velocity, transform.right);
		Vector3 yvel = Vector3.Project((Vector3)body.velocity, transform.up);

		if (Input.GetButtonDown("Jump") && currentJumps-->0){
			yvel = jumpSpeed * transform.up;
		}
		//body.velocity = rollingVelocity * transform.right + yvel * transform.up;

        //if (OnGround)
        //{
        //}
		    xvel = (xvel + Input.GetAxis("Horizontal") * hypeRate * Time.deltaTime * transform.right) * chillRate;
		body.velocity = xvel + yvel;
	}

	private void OnCollisionStay2D(Collision2D collision) {
		Vector2 groundAngle = collision.contacts[0].normal;
		//if (Vector3.Cross(groundAngle,transform.up).magnitude<.7f && (transform.up-(Vector3)groundAngle).magnitude> 1.2f) {
		if (IsGroundCollision(collision)) {
			//Vector3 xvel = Vector3.Project((Vector3)body.velocity, transform.right);
			//Vector3 yvel = Vector3.Project((Vector3)body.velocity, transform.up);
			
			transform.up = groundAngle;
            //body.velocity = xvel.magnitude * transform.right + yvel;
            currentJumps = maxJumps;
            currentStraightenTime = 0;
        }
	}

    private bool IsGroundCollision(Collision2D collision)
    {
        if (collision.contacts.Length == 0)
        {
            return false;
        }
        Vector2 groundAngle = collision.contacts[0].normal;
        return Vector3.Dot((Vector3)groundAngle, transform.up) > .5f  // on ramp or something
            || Vector3.Dot((Vector3)groundAngle, Vector3.up) > .5f;  // on ground
    }

    public void DamagePlayer(int damage)
    {
        if (!immune)
        {
            Health -= damage;
            if (Health <= 0)
            {
                DungeonManager.instance.Reset();
                DungeonManager.instance.LoadDungeon();
            }
            OnPlayerDamaged?.Invoke();
			StartCoroutine(IFrameRoutine());
        }
    }

	public float blinksPerSec;

	IEnumerator IFrameRoutine() {
		immune = true;
		float clockStart = Time.time;
		SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
		while (Time.time < clockStart + iFrameDuration) {
			spriteRenderer.enabled = (Time.time * blinksPerSec * 2f) % 2f > 1f;
			yield return null;
		}
		spriteRenderer.enabled = true;
		immune = false;
	}

	public System.Action OnPlayerDamaged;
}
