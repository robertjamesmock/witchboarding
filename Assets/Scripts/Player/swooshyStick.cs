﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swooshyStick : MonoBehaviour
{
    public GameObject fireSpellPrefab;
    public GameObject waterSpellPrefab;
    public GameObject lightningSpellPrefab;
    public GameObject rockSpellPrefab;
    public Color fireChargeColor = Color.red;
    public Color waterChargeColor = Color.blue;
    public Color lightningChargeColor = Color.yellow;
    public Color rockChargeColor = Color.grey;
    public float chargeScaleFactor = 0.33f;

    public int maxChargeLevel = 3;
    [HideInInspector]
    public List<Damage.DamageType> Charges;
    [HideInInspector]
    public Damage.DamageType LastAbsorbedElement => Charges[Charges.Count - 1];

    private Animation swingAnimation;
    private Color[] elementColors = new Color[Damage.NumDamageTypes];
    private GameObject[] elementSpellPrefabs = new GameObject[Damage.NumDamageTypes];

    // Use this for initialization
    void Awake () {
        Charges = new List<Damage.DamageType>();
        swingAnimation = GetComponent<Animation>();

        elementColors[(int)Damage.DamageType.Fire] = fireChargeColor;
        elementColors[(int)Damage.DamageType.Water] = waterChargeColor;
        elementColors[(int)Damage.DamageType.Lightning] = lightningChargeColor;
        elementColors[(int)Damage.DamageType.Rock] = rockChargeColor;

        elementSpellPrefabs[(int)Damage.DamageType.Fire] = fireSpellPrefab;
        elementSpellPrefabs[(int)Damage.DamageType.Water] = waterSpellPrefab;
        elementSpellPrefabs[(int)Damage.DamageType.Lightning] = lightningSpellPrefab;
        elementSpellPrefabs[(int)Damage.DamageType.Rock] = rockSpellPrefab;
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetButtonDown("Fire1")) {
            SwingBat();
		}

        if (Input.GetButtonDown("Fire2") && Charges.Count > 0 && !swingAnimation.isPlaying)
        {
            CastSpell();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (swingAnimation.isPlaying)
        {
            if (collision.TryGetComponent(out Enemy enemy))
            {
                enemy.TakeDamage(Damage.DamageType.Fire);
            }
            else if (collision.TryGetComponent(out Spell spell))
            {
                ChargeStick(spell.damageType);
                spell.End();
            }
        }
    }

    private void CastSpell()
    {
        var direction = Playboarder.Instance.batPointer.pointDirection;

        GameObject spellObj = Instantiate(
            elementSpellPrefabs[(int)LastAbsorbedElement],
            Playboarder.Instance.transform.position,
            Quaternion.FromToRotation(Vector3.right, direction)
            );

        Spell spell = spellObj.GetComponent<Spell>();

        Rigidbody2D spellBody = spellObj.GetComponent<Rigidbody2D>();
        spellBody.velocity = direction * spell.Speed;

        spellObj.transform.localScale *= 1 + ((Charges.Count - 1) * chargeScaleFactor);
        spellObj.transform.Translate(spell.StartDistance * Vector3.right);
        if (spell.PlayOnCast) spellObj.GetComponent<AudioTriggerer>().Play();

        Charges.Clear();
        UpdateStickColor();
        OnStickDischarged?.Invoke();
    }

    public void SwingBat()
    {
        swingAnimation.Play();
    }
        

    public void ChargeStick(Damage.DamageType damageType)
    {
        if (Charges.Count < maxChargeLevel)
        {
            Charges.Add(damageType);
        }
        UpdateStickColor();
        OnStickCharged?.Invoke(damageType);
    }

    void UpdateStickColor()
    {
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();
        if (Charges.Count == 0)
        {
            // uncharged stick is white
            sprite.color = Color.white;
        }
        else
        {
            Color elementColor = elementColors[(int)LastAbsorbedElement];
            sprite.color = elementColor + ((maxChargeLevel - Charges.Count) / (float)maxChargeLevel) * (Color.white - elementColor);
        }
    }

    public System.Action<Damage.DamageType> OnStickCharged;
	public System.Action OnStickDischarged;
}
