﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : MonoBehaviour {

    public Damage.DamageType damageType;
    public int Speed = 8;
    public float StartDistance = 1; // distance from stick that spell spawns
    public bool DestroyOnWall = true;
    public bool PlayOnCast = false;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Enemy enemy = collision.gameObject.GetComponent<Enemy>();

        if (enemy != null)
        {
            // hit enemy
            enemy.TakeDamage(damageType);
            End();
        }
        else
        {
            // hit wall
            if (DestroyOnWall) End();
        }
    }

    public void End()
    {
        Destroy(gameObject);
    }
}
