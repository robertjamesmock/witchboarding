﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentSingleton<T> : Singleton<T> where T : PersistentSingleton<T>
{
	protected override void InitSingleton() {
		base.InitSingleton();
		DontDestroyOnLoad(this);
	}
}
