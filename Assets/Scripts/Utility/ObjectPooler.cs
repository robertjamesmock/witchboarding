﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{

    static ObjectPooler _instance;

    static ObjectPooler Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameObject().AddComponent<ObjectPooler>();
            }
            return _instance;
        }
    }

    Dictionary<string, Pool> pools = new Dictionary<string, Pool>();

    private GameObject GetInstanceHelper(GameObject prefab)
    {
        if (pools.ContainsKey(prefab.name) && !pools[prefab.name].Empty())
        {
            GameObject obj = pools[prefab.name].Remove();
            Debug.Assert(!obj.activeSelf);
            obj.SetActive(true);
            return obj;
        }
        else
        {
            GameObject obj = Instantiate(prefab);
            obj.name = prefab.name;
            return obj;
        }
    }

    private void ReturnInstanceHelper(GameObject instance)
    {
        instance.SetActive(false);
        if (!pools.ContainsKey(instance.name))
        {
            //Debug.Log("making new object pool for " + instance.name);
            pools.Add(instance.name, new Pool());
        }
        pools[instance.name].Add(instance);
    }

    public static GameObject GetInstance(GameObject prefab)
    {
        return Instance.GetInstanceHelper(prefab);
    }

    public static void ReturnInstance(GameObject instance)
    {
        Instance.ReturnInstanceHelper(instance);
    }

    private class Pool
    {
        HashSet<GameObject> objSet;
        Queue<GameObject> objQueue;

        public Pool()
        {
            objSet = new HashSet<GameObject>();
            objQueue = new Queue<GameObject>();
        }

        public void Add(GameObject instance)
        {
            if (!objSet.Contains(instance))
            {
                objSet.Add(instance);
                objQueue.Enqueue(instance);
            }
        }

        public GameObject Remove()
        {
            if (Empty())
            {
                throw new System.InvalidOperationException("There are no objects in this poool.");
            }
            var obj = objQueue.Dequeue();
            Debug.Assert(objSet.Contains(obj));
            objSet.Remove(obj);
            return obj;
        }

        public bool Empty()
        {
            return !objQueue.Any();
        }
    }
}
