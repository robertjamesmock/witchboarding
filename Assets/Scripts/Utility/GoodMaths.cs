﻿using System;

public static class GoodMaths
{
    // Actual mod operator - returns a result in the range [0,b)
    public static int Mod(int a, int b)
	{
		return ((a % b) + b) % b;
	}
}
