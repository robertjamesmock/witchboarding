﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
	private static T _instance;

	public static T instance {
		get {
			if(_instance == null) {
				_instance = new GameObject().AddComponent<T>();
				_instance.InitSingleton();
			}
			return _instance;
		}
	}

	protected virtual void InitSingleton() {
		//Do jack shit by default
	}
}
