﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnWhenDead : MonoBehaviour
{
	public GameObject toSpawn;

	private void Start() {
		GetComponent<Enemy>().OnPlayerKill += Spawn;
	}

	void Spawn() {
		Instantiate(toSpawn, transform.position, Quaternion.identity);
	}
}
