﻿using UnityEditor;
using UnityEngine;

public class GoodAssets
{
    public static T LoadOrCreateAsset<T>(string path) where T : ScriptableObject
    {
        T asset = AssetDatabase.LoadAssetAtPath<T>(path);
        if (asset == null)
        {
            Debug.Log("creating new asset");
            asset = ScriptableObject.CreateInstance<T>();
        }
        return asset;
    }
}
