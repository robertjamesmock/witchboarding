﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GoodCollections
{
    /// <summary>
    /// Removes list[index] in O(1) time, but disturbs the order of the list
    /// </summary>
    public static void FastRemoveAt<T>(List<T> list, int index)
    {
        if (index < 0)
        {
            throw new System.ArgumentException("index must be > 0 but it was " + index);
        }
        if (list == null)
        {
            throw new System.ArgumentException("list cannot be null");
        }
        list[index] = list[list.Count - 1];
        list.RemoveAt(list.Count - 1);
    }
}
