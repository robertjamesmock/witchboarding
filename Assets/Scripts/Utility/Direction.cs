﻿using static GoodMaths;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    up,
    right,
    down,
    left
}

public static class DirectionExtension
{
    public static Point GetOffset(this Direction direction)
    {
        switch (direction)
        {
            case Direction.up:
                return new Point(0, 1);
            case Direction.right:
                return new Point(1, 0);
            case Direction.down:
                return new Point(0, -1);
            case Direction.left:
                return new Point(-1, 0);
            default:
                throw new System.Exception("not a real direction! :OOO");
        }
    }

    /// <summary>
    /// The direction that is clockwise of this direction
    /// </summary>
    public static Direction Clockwise(this Direction direction)
    {
        return (Direction) Mod((int)direction + 1, 4);
    }

    /// <summary>
    /// The direction that is counter-clockwise of this direction
    /// </summary>
    public static Direction CounterClockwise(this Direction direction)
    {
        return (Direction) Mod((int)direction - 1, 4);
    }

    /// <summary>
    /// The direction that is opposite this direction
    /// </summary>
    public static Direction Opposite(this Direction direction)
    {
        return (Direction) Mod((int)direction + 2, 4);
    }

    /// <summary>
    /// True for left and right, false for up and down
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    public static bool IsHorizontal(this Direction direction)
    {
        return ((int)direction) % 2 == 1;
    }

    /// <summary>
    /// True for up and down, false for left and right
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    public static bool IsVertical(this Direction direction)
    {
        return !direction.IsHorizontal();
    }

    public static string ToString(this Direction direction)
    {
        switch (direction)
        {
            case Direction.up:
                return "up";
            case Direction.down:
                return "down";
            case Direction.left:
                return "left";
            case Direction.right:
                return "right";
            default:
                throw new System.Exception("wtf direction is this?");
        }
    }
}
