﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Point one = new Point(1, 1);

    public static Point operator +(Point a, Point b) {
        return new Point(a.x + b.x, a.y + b.y);
    }

    public static Point operator -(Point a, Point b) {
        return new Point(a.x - b.x, a.y - b.y);
    }

	public static bool operator ==(Point a, Point b) {
		return a.x == b.x && a.y == b.y;
	}

	public static bool operator !=(Point a, Point b) {
		return !(a == b);
	}

    public static Point operator *(int i, Point p)
    {
        return new Point(i * p.x, i * p.y);
    }

    public static Point operator *(Point p, int i)
    {
        return i * p;
    }

    public static int Dot(Point a, Point b)
    {
        return a.x * b.x + a.y * b.y;
    }

    public int Dot(Point other)
    {
        return Dot(this, other);
    }

	public override int GetHashCode() {
		return (x.ToString() + " " + y.ToString()).GetHashCode();
	}

	public override bool Equals(object obj) {
		return (Point?)this == (obj as Point?);//if obj is not a point, cast will make it null, and this will never be.
	}

    public static implicit operator Vector2(Point p)
    {
        return new Vector2(p.x, p.y);
    }

    public override string ToString()
    {
        return "(" + x + ", " + y + ")";
    }
}