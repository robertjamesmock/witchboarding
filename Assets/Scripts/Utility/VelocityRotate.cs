﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityRotate : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        var body = GetComponent<Rigidbody2D>();
        var direction = body.velocity.normalized;
        transform.right = direction;
	}
}
