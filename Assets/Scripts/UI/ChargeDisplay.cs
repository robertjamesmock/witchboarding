﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChargeDisplay : MonoBehaviour {
    public GameObject ChargeIndicatorPrefab;

    private swooshyStick stick;
    private List<Image> chargeIndicators;

	private void Awake() {
		chargeIndicators = new List<Image>();
	}

	private void Start() {
        stick = Playboarder.Instance.stick;

		RectTransform rt = GetComponent<RectTransform>();
		rt.anchorMax = new Vector2(stick.maxChargeLevel * rt.anchorMax.x, rt.anchorMax.y);

		for (int i = 0; i < stick.maxChargeLevel; i++) {
			GameObject chargeIndicatorObj = Instantiate(
				ChargeIndicatorPrefab,
				transform);

			RectTransform rtChild = chargeIndicatorObj.GetComponent<RectTransform>();
			rtChild.anchorMin = new Vector2(i / (float)stick.maxChargeLevel, 0);
			rtChild.anchorMax = new Vector2((i + 1) / (float)stick.maxChargeLevel, 1);

            Image chargeIndicator = chargeIndicatorObj.GetComponent<Image>();
            chargeIndicator.color = Color.clear;
            chargeIndicators.Add(chargeIndicator);
		}
		stick.OnStickCharged += UpdateChargeDisplay;
		stick.OnStickDischarged += UpdateChargeDisplay;//yay operator overloading
	}
	
	void UpdateChargeDisplay(Damage.DamageType type) {
		UpdateChargeDisplay();
	}

	void UpdateChargeDisplay () {
		for(int i = 0; i < chargeIndicators.Count; i++)
        {
            Image chargeIndicator = chargeIndicators[i];
            if (i >= stick.Charges.Count)
            {
                chargeIndicator.color = Color.clear;
            }
            else
            {
                switch (stick.Charges[i])
                {
                    case Damage.DamageType.Fire:
                        chargeIndicator.color = Color.red;
                        break;
                    case Damage.DamageType.Water:
                        chargeIndicator.color = Color.blue;
                        break;
                    case Damage.DamageType.Lightning:
                        chargeIndicator.color = Color.yellow;
                        break;
                    case Damage.DamageType.Rock:
                        chargeIndicator.color = Color.grey;
                        break;
                    default:
                        Debug.Assert(false, "didn't cover all cases");
                        break;
                }
            }
        }
	}

	private void OnDestroy() {
        if (stick != null)
        {
            stick.OnStickCharged -= UpdateChargeDisplay;
            stick.OnStickDischarged -= UpdateChargeDisplay;
        }
	}
}
