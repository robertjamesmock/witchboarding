﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

	Playboarder player;
	RectTransform rt;

	// Use this for initialization
	void Start () {
		player = Playboarder.Instance;
		player.OnPlayerDamaged += HandlePlayerDamaged;
		rt = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void HandlePlayerDamaged () {
        rt.anchorMax = new Vector2(player.Health / (float)player.MaxHealth, rt.anchorMax.y);
	}

	private void OnDestroy() {
        if (player != null)
        {
		    player.OnPlayerDamaged -= HandlePlayerDamaged;
        }
	}
}
