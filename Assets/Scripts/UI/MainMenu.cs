﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	public void StartBigGame() {
        DungeonManager.instance.Set(DungeonType.Large);
		DungeonManager.instance.LoadDungeon();
	}

	public void StartSmolGame() {
        DungeonManager.instance.Set(DungeonType.Small);
        DungeonManager.instance.LoadDungeon();
    }
}
