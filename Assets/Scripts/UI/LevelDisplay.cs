﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelDisplay : MonoBehaviour
{
    TextMeshProUGUI textMesh;

    private void Awake()
    {
        textMesh = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        UpdateLevelDisplay();
    }

    void UpdateLevelDisplay()
    {
        textMesh.text = "Level " + DungeonManager.instance.Level;
    }

}
