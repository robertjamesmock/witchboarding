﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BulletFountain))]
public class FountainBulletTriggerer : AudioTriggerer {
    public GameObject bulletPrefab;
    private BulletFountain bulletFountain;

    protected override void OnStart()
    {
        bulletFountain = GetComponent<BulletFountain>();
    }

    public override void OnTrigger() {
        bulletFountain.Trigger(bulletPrefab, Vector2.up);
	}
}
