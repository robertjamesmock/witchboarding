﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    public System.Action OnPlayerKill;
    public Damage.DamageType type = Damage.DamageType.Fire;

    public void TakeDamage(Damage.DamageType type)
    {
        OnPlayerKill?.Invoke();
        Playboarder.Instance.stick.ChargeStick(this.type);
        Destroy(gameObject);
    }
}
