﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningBulletTriggerer : AudioTriggerer
{
    public GameObject BulletPrefab;

    private Enemy enemy;
    private GameObject nextBullet;
    private HashSet<GameObject> unfiredBullets;
    private Vector2 TowardPlayer => Playboarder.Instance.transform.position - transform.position;

    protected override void OnAwake()
    {
        unfiredBullets = new HashSet<GameObject>();
        enemy = GetComponent<Enemy>();
        enemy.OnPlayerKill += ReturnUnfiredBullets;
    }

    public override void OnTrigger()
    {
        if (nextBullet != null)
        {
            nextBullet.GetComponent<WallReflectTriggerer>().BounceOnNextTrigger(TowardPlayer);
        }

        // create the next bullet ahead of when we'll need it, so the bullet's AudioTriggerer has time to warm up
        QueueNewBullet();
    }

    private void ReturnUnfiredBullets()
    {
        foreach (GameObject bullet in unfiredBullets)
        {
            if (bullet != null)
            {
                ObjectPooler.ReturnInstance(bullet);
            }
        }
    }

    private void QueueNewBullet()
    {
        nextBullet = ObjectPooler.GetInstance(BulletPrefab);
        nextBullet.transform.position = transform.position;

        unfiredBullets.Add(nextBullet);

        WallReflectTriggerer bulletTriggerer = nextBullet.GetComponent<WallReflectTriggerer>();
        bulletTriggerer.OnFire =
            delegate (GameObject bullet)
            {
                unfiredBullets.Remove(bullet);
            };
        bulletTriggerer.Play();
    }

    private void OnDestroy()
    {
        enemy.OnPlayerKill -= ReturnUnfiredBullets;
    }
}
