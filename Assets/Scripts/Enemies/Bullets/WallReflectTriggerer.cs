﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallReflectTriggerer : AudioTriggerer
{
    public AudioClip[] ClipOptions;
    public Rigidbody2D Body;
    public int NumReflections = 1;
    public float Fastness = 0.5f;  // percentage of the (time of bounce) until the (time of next trigger) that it will take to reach next wall
    public GameObject ReflectionIndicatorLinePrefab;

    private Vector2 direction;
    private float outDistance;
    private bool readyToBounce;
    private ContactPoint2D[] contacts;
    private int reflectionCount;
    private TimedDrawLine reflectionIndicatorLine;

    // Called with this bullet's GameObject when this bullet fires
    public System.Action<GameObject> OnFire;

    public override void OnTrigger()
    {
        if (readyToBounce)
        {
            Body.constraints &= ~(RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY);
            Body.velocity = (1 / Fastness) * (outDistance / (float)TimeUntilNextTrigger) * direction;
            readyToBounce = false;
            OnFire?.Invoke(gameObject);
        }
    }

    protected override void OnAwake()
    {
        GetComponentInChildren<WallReflectCollider>().OnCollision += OnChildCollision;
    }

    private void OnEnable()
    {
        //OnFire = null;
        clip = ClipOptions[Random.Range(0, ClipOptions.Length - 1)];
        reflectionCount = 0;
        readyToBounce = false;
        contacts = new ContactPoint2D[5];
        reflectionIndicatorLine = ObjectPooler.GetInstance(ReflectionIndicatorLinePrefab).GetComponent<TimedDrawLine>();
    }

    private void OnChildCollision(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Playboarder>() != null)
        {
            // ignore player for this collision; we only care about walls
            return;
        }

        if (!readyToBounce)
        {
            if (reflectionCount < NumReflections)
            {
                collision.GetContacts(contacts);

                Vector2 wallNormal = Vector2.zero;
                for (int i = 0; i < collision.contactCount; i++)
                {
                    wallNormal += contacts[i].normal;
                }

                wallNormal = wallNormal.normalized;

                Vector2 nextDirection = Vector2.Reflect(direction, wallNormal).normalized;  // direction of reflection

                BounceOnNextTrigger(nextDirection);
                reflectionCount++;
            }
            else
            {
                EndMyLife();
            }
        }
    }

    public void BounceOnNextTrigger(Vector2 direction)
    {
        this.direction = direction.normalized;

        Body.velocity = Vector2.zero;
        Body.constraints |= RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;

        RaycastHit2D raycast = Physics2D.Raycast(Body.position, direction, 1000, 1 << LayerMask.NameToLayer("Walls"));
        if (raycast.collider == null)
        {
            // nowhere to reflect to
            EndMyLife();
        }

        outDistance = raycast.distance;
        DrawReflectionLine();
        readyToBounce = true;
    }

    private void DrawReflectionLine()
    {
        //Debug.DrawLine(Body.position, (outDistance * direction) + Body.position, Color.yellow, (float)TimeUntilNextTrigger);
        reflectionIndicatorLine.Draw(Body.position, (outDistance * direction) + Body.position, (float)TimeUntilNextTrigger);
    }

    private void EndMyLife()
    {
        if (reflectionIndicatorLine != null)
        {
            ObjectPooler.ReturnInstance(reflectionIndicatorLine.gameObject);
        }

        ObjectPooler.ReturnInstance(gameObject);
    }
}
