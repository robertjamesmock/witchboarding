﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDrawLine : MonoBehaviour
{
    private SpriteRenderer sprite;
    private float startTime;
    private float timer;

    private void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        sprite.color *= new Color(1, 1, 1, 0);  // make line transparent
        startTime = 0;
        timer = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer < startTime)
        {
            sprite.color += new Color(0, 0, 0, Time.deltaTime / startTime / 2);
        }
        else
        {
            sprite.color *= new Color(1, 1, 1, 0);  // make line transparent
        }
    }

    public void Draw(Vector2 start, Vector2 end, float duration)
    {
        startTime = duration;
        timer = 0;

        Vector2 startToEnd = end - start;
        transform.position = start + (startToEnd / 2);
        transform.localScale = new Vector3(startToEnd.magnitude, transform.localScale.y, transform.localScale.z);
        transform.right = startToEnd.normalized;

        sprite.color *= new Color(1, 1, 1, 0);  // make line transparent
    }
}
