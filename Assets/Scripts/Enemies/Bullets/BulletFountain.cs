﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFountain : MonoBehaviour
{
    public int numBullets = 5;
    public float conePortion = .125f;

    public void Trigger(GameObject bulletPrefab, Vector2 direction)
    {
        for (int i = 0; i < numBullets; i++)
        {
            float rads = Random.Range(-conePortion * 2 * Mathf.PI, conePortion * 2 * Mathf.PI) + (Mathf.PI / 2);
            Vector2 angle = new Vector2(Mathf.Cos(rads), Mathf.Sin(rads));
            angle = Quaternion.FromToRotation(Vector3.up, direction) * angle;

            GameObject bullet = ObjectPooler.GetInstance(bulletPrefab);
            bullet.transform.position = transform.position;
            bullet.transform.rotation = Quaternion.FromToRotation(Vector3.right, angle);

            bullet.GetComponent<Rigidbody2D>().velocity = angle * bullet.GetComponent<bullet>().speed;
        }
    }
}
