﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour {

    public Damage.DamageType damageType = Damage.DamageType.Fire;
    public int damage = 1;
    public float speed = 10f;
    public bool DestroyOnWallCollision = true;
    public bool freezeRotation = true;

    private void Awake()
    {
        gameObject.GetComponent<Rigidbody2D>().freezeRotation = freezeRotation;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject collider = collision.gameObject;
        Playboarder playborder = collider.GetComponent<Playboarder>();

        if (playborder != null)
        {
            // hit player
            playborder.DamagePlayer(damage);
            ObjectPooler.ReturnInstance(gameObject);
        }
        else
        {
            // hit wall
            HitWall(collision);
        }
    }

    protected virtual void HitWall(Collision2D collision)
    {
        if (DestroyOnWallCollision)
            ObjectPooler.ReturnInstance(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //stickDeflect(collision);

        stickAbsorb(collision);
    }

    void stickAbsorb(Collider2D collision)
    {
        swooshyStick stick = collision.gameObject.GetComponent<swooshyStick>();

        if (stick != null && stick.GetComponent<Animation>().isPlaying)
        {
            stick.ChargeStick(damageType);
            ObjectPooler.ReturnInstance(gameObject);
        }
    }

    void stickDeflect(Collider2D collision)
    {
		Debug.Assert(false, "this shouldn't be called any more");
        swooshyStick stick = collision.gameObject.GetComponent<swooshyStick>();

        if (stick != null && stick.GetComponent<Animation>().isPlaying)
        {
            // deflected by stick
            Rigidbody2D body = GetComponent<Rigidbody2D>();

            var deflectionDirection = Playboarder.Instance.batPointer.pointDirection;
            body.velocity = deflectionDirection * body.velocity.magnitude;
            transform.rotation = Quaternion.FromToRotation(Vector2.right, deflectionDirection);

            gameObject.layer = LayerMask.NameToLayer("PlayerBullets");
        }
    }
}
