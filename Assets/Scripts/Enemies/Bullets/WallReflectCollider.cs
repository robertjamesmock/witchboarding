﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallReflectCollider : MonoBehaviour
{
    public System.Action<Collision2D> OnCollision;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnCollision(collision);
    }
}
