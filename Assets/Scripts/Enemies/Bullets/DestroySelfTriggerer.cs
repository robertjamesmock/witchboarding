﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class DestroySelfTriggerer : AudioTriggerer
{
    public NoteLocation FadeTimeBeats;
    public NoteLocation minActiveBeats;
    private SpriteRenderer sprite;
    private float fadeTime;
    private float currentFadeTime;
    private bool fading;
    private float ogAlpha;
    private float startOfMyLife;

    protected override void OnAwake() {
        sprite = GetComponent<SpriteRenderer>();
        ogAlpha = sprite.color.a;
    }

    private void OnEnable()
    {
        fadeTime = (float)AudioTriggerManager.Instance.DspTimeInLoopFromNoteLocation(FadeTimeBeats);
        startOfMyLife = Time.time;
        currentFadeTime = fadeTime;
        fading = false;
        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, ogAlpha);
    }

    protected override void OnUpdate()
    {
        if (fading)
        {
            currentFadeTime -= Time.deltaTime;
            sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, ogAlpha * currentFadeTime / fadeTime);
            if (currentFadeTime <= 0)
            {
                fading = false;
                ObjectPooler.ReturnInstance(gameObject);
            }
        }
    }

    public override void OnTrigger()
    {
        fading = (Time.time - startOfMyLife > AudioTriggerManager.Instance.DspTimeInLoopFromNoteLocation(minActiveBeats));
    }
}
