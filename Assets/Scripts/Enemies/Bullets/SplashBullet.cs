﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BulletFountain))]
public class SplashBullet : bullet
{
    public int depth;
    BulletFountain bulletFountain;

    private void Start()
    {
        bulletFountain = GetComponent<BulletFountain>();
        //transform.localScale = (depth + 1) / 3f * Vector3.one;
        //speed = (depth + 1)
    }

    protected override void HitWall(Collision2D collision)
    {
        if (depth > 0)
        {
            string parentName = gameObject.name;

            depth--;
            transform.localScale /= 1.5f;
            speed /= 2.5f;
            gameObject.name = "splashbullet" + depth + "scale" + transform.localScale + "speed" + speed;

            bulletFountain.Trigger(gameObject, collision.GetContact(0).normal);

            depth++;
            transform.localScale *= 1.5f;
            speed *= 2.5f;
            gameObject.name = parentName;
        }
        ObjectPooler.ReturnInstance(gameObject);
    }
}
