﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockBulletTriggerer : AudioTriggerer
{
    public GameObject bulletPrefab;
    public float bulletSpeed = 10;

    public override void OnTrigger()
    {
        Vector3 playerPosition = Playboarder.Instance.transform.position;
        Vector2 angle = (playerPosition - transform.position).normalized;

        GameObject bullet = ObjectPooler.GetInstance(bulletPrefab);
        bullet.transform.position = transform.position;
        bullet.transform.rotation = Quaternion.FromToRotation(Vector3.right, angle);

        Rigidbody2D body = bullet.GetComponent<Rigidbody2D>();
        body.AddForce(angle * bulletSpeed, ForceMode2D.Impulse);
        bullet.GetComponent<AudioTriggerer>().Play();
    }
}
