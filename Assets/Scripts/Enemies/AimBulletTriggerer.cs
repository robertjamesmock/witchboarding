﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimBulletTriggerer : AudioTriggerer
{
    public GameObject bulletPrefab;
    public float bulletSpeed = 10;
    public bool playBulletOnFire = false;

    public override void OnTrigger()
    {
        Vector3 playerPosition = Playboarder.Instance.transform.position;
        Vector2 angle = (playerPosition - transform.position).normalized;

        GameObject bullet = ObjectPooler.GetInstance(bulletPrefab);
            bullet.transform.position = transform.position;
            bullet.transform.rotation = Quaternion.FromToRotation(Vector3.right, angle);

        Rigidbody2D body = bullet.GetComponent<Rigidbody2D>();
        body.velocity = angle * bulletSpeed;
        if (playBulletOnFire) bullet.GetComponent<AudioTriggerer>().Play();
    }
}
