﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;

public class RoomCollectionMetadata : ScriptableObject
{
    public const string LOADABLE_ROOM_COLLECTION_ASSET_PATH = "Assets/Resources/RoomCollectionMeta.asset";
    public const string BOSS_ROOM_COLLECTION_ASSET_PATH = "Assets/Resources/BossRoomCollectionMeta.asset";

    [SerializeField]
    private List<PairBuildIndexRoomMetadata> _serializedBuildIndexToRoomMetadata;
    private Dictionary<int, RoomMetadata> _cachedBuildIndexToRoomMetadata;

    [Serializable]
    public class PairBuildIndexRoomMetadata
    {
        public int buildIndex;
        public RoomMetadata roomMetadata;

        public PairBuildIndexRoomMetadata(int i, RoomMetadata rm)
        {
            this.buildIndex = i;
            this.roomMetadata = rm;
        }
    }

    public IDictionary<int, RoomMetadata> BuildIndexToRoomMetadata
    {
        get
        {
            if (_serializedBuildIndexToRoomMetadata == null)
            {
                _serializedBuildIndexToRoomMetadata = new List<PairBuildIndexRoomMetadata>();
            }
            if (_cachedBuildIndexToRoomMetadata == null)
            {
                // load the key/value pairs into the cache
                _cachedBuildIndexToRoomMetadata = new Dictionary<int, RoomMetadata>();
                foreach (var pair in _serializedBuildIndexToRoomMetadata)
                {
                    _cachedBuildIndexToRoomMetadata.Add(pair.buildIndex, pair.roomMetadata);
                }
            }
            return _cachedBuildIndexToRoomMetadata;
        }
    }

    public static RoomCollectionMetadata RuntimeLoad(string path)
    {
        string name = Path.GetFileNameWithoutExtension(path);
        var result = Resources.Load<RoomCollectionMetadata>(name);
        if (result == null)
        {
            throw new System.InvalidOperationException("no room collection metadata found!");
        }
        return result;
    }

    public static RoomCollectionMetadata EditorLoad(string path)
    {
        RoomCollectionMetadata result = AssetDatabase.LoadAssetAtPath<RoomCollectionMetadata>(path);
        if (result == null)
        {
            result = CreateInstance<RoomCollectionMetadata>();
        }
        return result;
    }

    public void Save(string path)
    {
        // persist the cached key/value pairs to the serializable list
        _serializedBuildIndexToRoomMetadata = new List<PairBuildIndexRoomMetadata>();
        foreach (var kv in BuildIndexToRoomMetadata)
        {
            _serializedBuildIndexToRoomMetadata.Add(new PairBuildIndexRoomMetadata(kv.Key, kv.Value));
        }
        if (!AssetDatabase.Contains(this))
        {
            AssetDatabase.CreateAsset(this, path);
        }
    }
}
