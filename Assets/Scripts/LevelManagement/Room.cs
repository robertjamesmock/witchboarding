﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RoomType
{
    Starting,
    Loadable,
    Boss
}

public class Room : MonoBehaviour {
	public const float SEG_WIDTH = 3f;
	public const float SEG_HEIGHT = 3f;
    public const float ACTIVATION_DISTANCE = 20f;
    //public bool startingRoom = false;
    public RoomType roomType = RoomType.Starting;

	private bool entered = false;

    [SerializeField]
    public RoomSegmentColumn[] roomSegmentColumns;

    public RoomSegment this[int i, int j]
    {
        get { return roomSegmentColumns[i][j]; }
        set { roomSegmentColumns[i][j] = value; }
    }

    public int Width => roomSegmentColumns == null ? 0 : roomSegmentColumns.Length;
    public int Height => Width == 0 ? 0 : roomSegmentColumns[0].Length;
    public Point coordinates;  // coordinates of the bottom-left roomSegment in this room

    public AudioTriggerer[] triggerers = new AudioTriggerer[0];
    private void Start()
    {
        Debug.Assert(roomSegmentColumns != null, "create a room segment structure plz");
        foreach (RoomSegmentColumn row in roomSegmentColumns)
        {
            foreach (RoomSegment segment in row)
            {
                foreach (Entrance entrance in segment)
                {
                    entrance.myRoom = this;
                    entrance.myRoomSegment = segment;
                }
            }
        }
        if (roomType == RoomType.Starting)
        {
            RoomManager.SetStartingRoom(GetComponent<Room>());
            RoomEnter();
        }
    }

    private void Update()
    {
        if ((Playboarder.Instance.transform.position - transform.position).magnitude < ACTIVATION_DISTANCE)
        {
            RoomEnter();
        }
    }

    public void RoomEnter()
    {
        if (entered) return;
        entered = true;
        foreach (AudioTriggerer triggerer in triggerers)
        {
            if (triggerer != null && triggerer.gameObject.activeInHierarchy)
            {
                triggerer.Play();
            }
        }
        foreach (RoomSegmentColumn row in roomSegmentColumns)
        {
            foreach (RoomSegment segment in row)
            {
                foreach (Entrance entrance in segment)
                {
                    if (entrance.ConnectedEntrance == null)
                    {
                        RoomManager.LoadRoomAndAttachToEntrance(entrance);
                    }
                }
            }
        }
    }

#if UNITY_EDITOR
    private Vector2 BotLeft => (Vector2)transform.position - (Width / 2f) * Vector2.right * SEG_WIDTH - (Height / 2f) * Vector2.up * SEG_HEIGHT;

    public void CollectTriggerers() {
		triggerers = FindObjectsOfType<AudioTriggerer>();
	}

	public void MakeDoor(int x, int y, Direction direction) {
        if (this[x, y][direction] != null)
        {
            throw new System.Exception("there is already a door here!");
        }

		var doorClone = (GameObject)Instantiate(UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/RoomBuilding/door.prefab", typeof(GameObject)));
        doorClone.transform.position = BotLeft + (SEG_WIDTH * (new Vector2(x, y) + (0.5f * (Vector2.one + direction.GetOffset()))));
        doorClone.transform.localScale = new Vector3(SEG_WIDTH, doorClone.transform.localScale.y, 1);
        doorClone.transform.up = (Vector2)direction.GetOffset();
        Entrance entrance = doorClone.GetComponent<Entrance>();
        entrance.myRoomSegment = this[x, y];
        entrance.myRoom = this;
        entrance.direction = direction;
        this[x, y][direction] = entrance;
	}

	public void RemoveDoor(int x, int y, Direction direction) {
        if (this[x, y][direction] != null)
        {
            DestroyImmediate(this[x, y][direction].gameObject);
            this[x, y][direction] = null;
        }
	}

    public void EnsureMinimumGrid()
    {
        if (roomSegmentColumns == null || roomSegmentColumns.Length == 0 || roomSegmentColumns[0].Length == 0)
        {
            roomSegmentColumns = new RoomSegmentColumn[1];
            roomSegmentColumns[0] = new RoomSegmentColumn(1);
        }
    }

	public void EnsureGrid(int width, int height) {
        if (width != Width || height != Height)
        {
            // Destroy all entrances
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    RoomSegment roomSegment = this[i, j];
                    for (int k = 0; k < 4; k++)
                    {
                        Entrance entrance = roomSegment[(Direction)k];
                        if (entrance != null)
                        {
                            DestroyImmediate(entrance.gameObject);
                        }
                    }
                }
            }

            // Create new RoomSegments corresponding to the new room size
            roomSegmentColumns = new RoomSegmentColumn[width];
            for (int i = 0; i < width; i++)
            {
                roomSegmentColumns[i] = new RoomSegmentColumn(height);
                for (int j = 0; j < height; j++)
                {
                    roomSegmentColumns[i][j].coordinates = new Point(i, j);
                }
            }
        }
        Debug.Assert(Width == width);
        Debug.Assert(Height == height);
	}


	private void OnDrawGizmosSelected() {
		for (int x = 0; x < Width; x++) {
			for (int y = 0; y < Height; y++) {
				Gizmos.DrawWireCube((Vector3)BotLeft + (x + .5f) * SEG_WIDTH * Vector3.right + (y + .5f) * SEG_HEIGHT * Vector3.up, new Vector3(SEG_WIDTH,SEG_HEIGHT,0));
			}
		}

	}
#endif

    [System.Serializable]
	public class RoomSegment : IEnumerable<Entrance> {
        public Point coordinates;

        // do not rename these fields or shit will break 'cause Unity sux
        public Entrance UpDog;
		public Entrance RightDoor;
		public Entrance DownPortal;
		public Entrance LeftEntrance;

		public Entrance this[Direction direction]
        {
            get
            {
                switch (direction)
                {
                    case Direction.up:
                        return UpDog;
                    case Direction.right:
                        return RightDoor;
                    case Direction.down:
                        return DownPortal;
                    case Direction.left:
                        return LeftEntrance;
                    default:
                        throw new System.Exception("what the fuck direction did you give me");
                }
            }
            set
            {
                switch (direction)
                {
                    case Direction.up:
                        UpDog = value;
                        break;
                    case Direction.right:
                        RightDoor = value;
                        break;
                    case Direction.down:
                        DownPortal = value;
                        break;
                    case Direction.left:
                        LeftEntrance = value;
                        break;
                    default:
                        throw new System.Exception("what the fuck direction did you give me");
                }
            }
        }

        public IEnumerator<Entrance> GetEnumerator()
        {
            List<Entrance> entrances = new List<Entrance>();
            if (UpDog != null) entrances.Add(UpDog);
            if (RightDoor != null) entrances.Add(RightDoor);
            if (DownPortal != null) entrances.Add(DownPortal);
            if (LeftEntrance != null) entrances.Add(LeftEntrance);
            return entrances.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
	
	[System.Serializable]
	public class RoomSegmentColumn {
		public RoomSegment[] segments = new RoomSegment[0];
		public RoomSegment this[int i] {
			get { return segments[i];}
			set { segments[i] = value; }
		}
        public int Length => segments.Length;
		public IEnumerator GetEnumerator() { return segments.GetEnumerator(); }

		public RoomSegmentColumn(int length) {
			segments = new RoomSegment[length];
			for (int i = 0; i < length; i++) {
				segments[i] = new RoomSegment();
			}
		}
	}
}
