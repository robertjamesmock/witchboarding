﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitPortal : MonoBehaviour
{
	private void OnTriggerEnter2D(Collider2D collision) {
		if (collision.GetComponent<Playboarder>()) {
			StartCoroutine(AnimateExit());
		}
	}

	private void Update() {
		transform.Rotate(Vector3.forward);
	}

	IEnumerator AnimateExit() {
		Camera cam = Camera.main;
		cameraFollow follow = cam.GetComponent<cameraFollow>();
		Playboarder.Instance.GetComponent<Rigidbody2D>().gravityScale = 0;
        Playboarder.Instance.immune = true;
		for (int i = 0; i < 100; i++) {
			Playboarder.Instance.transform.position = (Playboarder.Instance.transform.position*7f + transform.position) / 8f;
			cam.orthographicSize *= .995f;
			cam.transform.position += Random.insideUnitSphere * .3f;
			yield return null;
		}
        DungeonManager.instance.IncreaseDungeonSize();
        DungeonManager.instance.LoadDungeon();
	}
}
