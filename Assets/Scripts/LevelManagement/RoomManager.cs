﻿using static System.Math;
using static GoodCollections;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class RoomManager : Singleton<RoomManager>
{
    public const int MAX_ROOM_SIZE = 8;

    private HashSet<Point> inhabitedSquares = new HashSet<Point>();
    private IDictionary<int, RoomMetadata> loadableRoomBuildIndexToRoomMetadata;
    private IDictionary<int, RoomMetadata> bossRoomBuildIndexToRoomMetadata;
    private Scene lastLoadedScene;
    private int roomCount;
    private bool loadedBossRoom;

    #region initialization
    protected override void InitSingleton()
    {
        base.InitSingleton();
        RoomCollectionMetadata rcm = RoomCollectionMetadata.RuntimeLoad(RoomCollectionMetadata.LOADABLE_ROOM_COLLECTION_ASSET_PATH);
        RoomCollectionMetadata brcm = RoomCollectionMetadata.RuntimeLoad(RoomCollectionMetadata.BOSS_ROOM_COLLECTION_ASSET_PATH);
        loadableRoomBuildIndexToRoomMetadata = rcm.BuildIndexToRoomMetadata;
        bossRoomBuildIndexToRoomMetadata = brcm.BuildIndexToRoomMetadata;
        roomCount = 0;
        loadedBossRoom = false;
    }
    #endregion

    #region public methods
    public static void SetStartingRoom(Room room)
    {
        if (instance.roomCount != 0)
        {
            Debug.Assert(instance.inhabitedSquares.Count > 0, "room manager inner state is broken - currentRooms > 0 but no inhabited squares");
            foreach (Point p in instance.inhabitedSquares)
            {
                Debug.Log(p);
            }
            throw new System.InvalidOperationException("There are already rooms loaded - too late to set the starting room");
        }
        Debug.Assert(instance.inhabitedSquares.Count == 0, "room manager inner state is broken - currentRooms = 0 but there are inhabited squares");
        instance.roomCount++;
        for (int i = 0; i < room.Width; i++)
        {
            for (int j = 0; j < room.Height; j++)
            {
                instance.inhabitedSquares.Add(new Point(i, j));
            }
        }
    }

    public static void LoadRoomAndAttachToEntrance(Entrance entrance)
    {
        instance.StartCoroutine(instance.LoadRoomAndAttachToEntranceRoutine(entrance));
    }
    #endregion

    #region private methods
    private void HandleSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        lastLoadedScene = arg0;
    }

    IEnumerator LoadRoomAndAttachToEntranceRoutine(Entrance entrance)
    {
        //Debug.Log("Trying to attach a room to an entrance facing " + entrance.direction);
        Point worldEntranceCoords = entrance.Coordinates + entrance.myRoom.coordinates;
        Pyramid availableSpace = GetAvailableSpace(worldEntranceCoords, entrance.direction);
        //Debug.Log(availableSpace.ToString());

        int buildIndex;
        RoomMetadata.DoorOffset doorOffset;
        if (!loadedBossRoom && FindRoomToFit(availableSpace, entrance.direction, out doorOffset, out buildIndex))
        {
            //Debug.Log("found a room! :D");
            roomCount++;

            // mark the squares taken up by the room as inhabited
            foreach (Point worldP in GetWorldPoints(worldEntranceCoords, doorOffset))
            {
                if (inhabitedSquares.Contains(worldP))
                {
                    throw new System.Exception(worldP + " is already occupied by a room");
                }
                inhabitedSquares.Add(worldP);
                //Debug.Log(worldP);
            }

            // we found a room that fits, so load the scene and connect the entrances
            SceneManager.sceneLoaded += HandleSceneLoaded;
            yield return SceneManager.LoadSceneAsync(buildIndex, LoadSceneMode.Additive);

            ConnectLastLoadedSceneToEntrance(entrance, worldEntranceCoords, doorOffset);
        }
        else
        {
            // There are no rooms that fit, so close off the entrance and put up a wall
            //Debug.Log("didn't find a room");
            entrance.Close();
        }
    }

    private void ConnectLastLoadedSceneToEntrance(Entrance entrance, Point worldEntranceCoords, RoomMetadata.DoorOffset doorOffset)
    {
        // find the Room script within the scene
        Room room = null;
        GameObject[] rootObjects = lastLoadedScene.GetRootGameObjects();
        foreach (GameObject obj in rootObjects)
        {
            Room maybeRoom = obj.GetComponent<Room>();
            if (maybeRoom != null)
            {
                room = maybeRoom;
                break;
            }
        }
        Debug.Assert(room != null, "this room doesn't have a room in it! :O");

        // connect the entrance to the opposing entrance
        var roomSegmentOfOpposingEntrance = room[doorOffset.segmentIndex.x, doorOffset.segmentIndex.y];
        Entrance opposingEntrance = roomSegmentOfOpposingEntrance[doorOffset.doorDirection];
        entrance.ConnectedEntrance = opposingEntrance;
        opposingEntrance.ConnectedEntrance = entrance;

        // move the room so the entrances line up
        Vector3 physicalOffset = entrance.transform.position - opposingEntrance.transform.position;
        foreach (GameObject obj in rootObjects)
        {
            obj.transform.position += physicalOffset;
        }

        // set the coordinates of the room
        Point worldOpposingEntranceCoords = worldEntranceCoords + entrance.direction.GetOffset();
        room.coordinates = worldOpposingEntranceCoords - opposingEntrance.Coordinates;
    }

    /// <summary>
    /// Returns the set of points in world space that would be taken up by the room corresponding to the given doorOffset,
    /// should the room be connected at the startingCoordinates.
    /// </summary>
    /// <param name="startingCoordinates"></param>
    /// <param name="direction"></param>
    /// <param name="doorOffset"></param>
    /// <returns></returns>
    private HashSet<Point> GetWorldPoints(Point startingCoordinates, RoomMetadata.DoorOffset doorOffset)
    {
        HashSet<Point> worldPoints = new HashSet<Point>();
        Direction direction = doorOffset.doorDirection.Opposite();

        for (int i = -1 * doorOffset.clockwiseWidth; i <= doorOffset.counterClockwiseWidth; i++)
        {
            for (int j = 1; j <= doorOffset.height; j++)
            {
                Point p = new Point(-1 * i, j);
                Direction dirX, dirY;
                if (direction.IsHorizontal())
                {
                    dirX = direction.CounterClockwise();
                    dirY = direction.Opposite();
                }
                else
                {
                    dirX = direction.Clockwise();
                    dirY = direction;
                }
                Point worldP = startingCoordinates + new Point(
                        p.Dot(dirX.GetOffset()),
                        p.Dot(dirY.GetOffset()));
                worldPoints.Add(worldP);
            }
        }

        return worldPoints;
    }

    private Pyramid GetAvailableSpace(Point startingCoordinates, Direction direction)
    {
        Pyramid pyramid = new Pyramid();
        Color color = Random.ColorHSV();
        color.a = 1f;

        // find height of central column
        pyramid[0] = FindHeight(startingCoordinates, direction, MAX_ROOM_SIZE);

        // find clockwise heights
        for (int column = 1; column <= MAX_ROOM_SIZE; column++)
        {
            int prevHeight = pyramid[column - 1];
            int height = FindHeight(startingCoordinates + column * direction.Clockwise().GetOffset(), direction, prevHeight);
            if (height == 0) break;
            pyramid[column] = height;
        }

        // find counter-clockwise heights
        for (int column = -1; column >= -1 * MAX_ROOM_SIZE; column--)
        {
            int prevHeight = pyramid[column + 1];
            int height = FindHeight(startingCoordinates + column * direction.Clockwise().GetOffset(), direction, prevHeight);
            if (height == 0) break;
            pyramid[column] = height;
        }

        return pyramid;
    }

    private int FindHeight(Point startingCoordinates, Direction direction, int maxHeight)
    {
        if (maxHeight == 0)
        {
            return 0;
        }
        for (int height = 1; height <= maxHeight; height++)
        {
            Point coord = startingCoordinates + height * direction.GetOffset();
            if (inhabitedSquares.Contains(coord) || !FitsInLevel(coord))
            {
                return height - 1;
            }
        }
        return maxHeight;
    }

    private bool FitsInLevel(Point coord)
    {
        Point min = DungeonManager.instance.NegativeBounds;
        Point max = DungeonManager.instance.PositiveBounds;
        return coord.x > min.x && coord.x < max.x && coord.y > min.y && coord.y < max.y;
    }

    /// <summary>
    /// Finds a random room that fits in the available space.
    /// </summary>
    /// <returns><c>true</c>, if a room was found, <c>false</c> if no rooms fit.</returns>
    /// <param name="availableSpace">Available space.</param>
    /// <param name="direction">Direction of the entrance we want to fit a room onto.</param>
    /// <param name="doorOffset">The doorOffset corresponding to the opposing entrance we found.</param>
    /// <param name="buildIndex">The build index of the scene corresponding to the room we found.</param>
    private bool FindRoomToFit(Pyramid availableSpace, Direction direction, out RoomMetadata.DoorOffset doorOffset, out int buildIndex)
    {
        doorOffset = null;
        buildIndex = -1;
        if (availableSpace[0] == 0)
        {
            // There is no space at all, so we couldn't possibly find a viable room
            return false;
        }

        IRoomOrder roomOrder = new RandomRoomOrder(RoomType.Loadable, direction.Opposite());
        if (roomCount >= DungeonManager.instance.MaxRooms)
        {
            // ready for a boss room, so we prioritize boss rooms in the room order
            roomOrder = new ConcatenateRoomOrder(new RandomRoomOrder(RoomType.Boss, direction.Opposite()), roomOrder);
        }

        foreach (var roomInfo in roomOrder)
        {
            buildIndex = roomInfo.buildIndex;
            doorOffset = roomInfo.doorOffset;
            if (availableSpace.Fits(doorOffset.height, doorOffset.clockwiseWidth, doorOffset.counterClockwiseWidth))
            {
                // we found a viable room and a viable entrance in the room!
                loadedBossRoom = roomInfo.roomType == RoomType.Boss;
                return true;
            }
        }

        // We went through all possibilities and didn't find a viable room
        return false;
    }
    #endregion

    #region Private classes
    private interface IRoomOrder : IEnumerable<RoomInfo> { }

    private class RoomInfo
    {
        public readonly int buildIndex;
        public readonly RoomMetadata.DoorOffset doorOffset;
        public readonly RoomType roomType;

        public RoomInfo(int buildIndex, RoomMetadata.DoorOffset doorOffset, RoomType roomType)
        {
            this.doorOffset = doorOffset ?? throw new ArgumentNullException(nameof(doorOffset));
            this.buildIndex = buildIndex;
            this.roomType = roomType;
        }
    }

    private class ConcatenateRoomOrder : IRoomOrder
    {
        IRoomOrder first;
        IRoomOrder second;

        // Creates a new room order that returns all the rooms in the first IRoomOrder,
        // and then returns all the rooms in the second IRoomOrder
        public ConcatenateRoomOrder(IRoomOrder first, IRoomOrder second)
        {
            this.first = first;
            this.second = second;
        }

        public IEnumerator<RoomInfo> GetEnumerator()
        {
            foreach (var room in first)
            {
                yield return room;
            }
            foreach (var room in second)
            {
                yield return room;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    private class RandomRoomOrder : IRoomOrder
    {
        Direction direction;
        RoomType roomType;
        IDictionary<int, RoomMetadata> buildIndexToRoomMetadata;

        // Creates a new room order that randomly returns rooms in the passed dictionary
        // Returns rooms in a random order, and only returns rooms facing the passed direction
        public RandomRoomOrder(RoomType roomType, Direction direction)
        {
            this.direction = direction;
            this.roomType = roomType;
            switch (roomType)
            {
                case (RoomType.Loadable):
                    this.buildIndexToRoomMetadata = instance.loadableRoomBuildIndexToRoomMetadata;
                    break;
                case (RoomType.Boss):
                    buildIndexToRoomMetadata = instance.bossRoomBuildIndexToRoomMetadata;
                    break;
                case (RoomType.Starting):
                    throw new Exception("shouldn't be trying to load starting rooms");
            }
        }

        public IEnumerator<RoomInfo> GetEnumerator()
        {
            List<int> possibleBuildIndices = new List<int>(buildIndexToRoomMetadata.Keys);
            while (possibleBuildIndices.Count > 0)
            {
                // as song as there are more rooms to try, pick a random room
                int i = Random.Range(0, possibleBuildIndices.Count);
                int buildIndex = possibleBuildIndices[i];
                if (buildIndex == -1)
                {
                    throw new System.Exception("encountered a room that isn't in the build settings!");
                }

                FastRemoveAt(possibleBuildIndices, i);  // so that we don't return this room again
                RoomMetadata roomMetadata = buildIndexToRoomMetadata[buildIndex];
                List<RoomMetadata.DoorOffset> possibleDoorOffsets = new List<RoomMetadata.DoorOffset>(
                    roomMetadata.doorOffsets[direction]);

                while (possibleDoorOffsets.Count > 0)
                {
                    // as long as the room has more doors to try, pick a random door within that room
                    int j = Random.Range(0, possibleDoorOffsets.Count);
                    var doorOffset = possibleDoorOffsets[j];
                    FastRemoveAt(possibleDoorOffsets, j);  // so that we don't return this doorOffset again
                    yield return new RoomInfo(buildIndex, doorOffset, roomType);
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    private class Pyramid
    {
        readonly int[] clockwiseDistances;  // heights of pyramid columns to the right, including central column
        readonly int[] counterClockwiseDistances;  // heights of pyramid columnds to the left, excluding central column

        public Pyramid()
        {
            clockwiseDistances = new int[MAX_ROOM_SIZE + 1];
            counterClockwiseDistances = new int[MAX_ROOM_SIZE];
        }

        /// <summary>
        /// Access the height at a particular column. Column 0 is the central column,
        /// positive indices are clockwise of the central column, and negative
        /// indices are counter-clockwise of the central column
        /// </summary>
        public int this[int column]
        {
            get
            {
                if (column >= 0)
                {
                    return clockwiseDistances[column];
                }
                else
                {
                    return counterClockwiseDistances[(column + 1) * -1];
                }
            }
            set
            {
                if (column != 0 && value > this[Sign(column) * (Abs(column) - 1)])
                {
                    string message = "the height of a column cannot be greater than the height of it's adjacent column that is closer to the center of the pyramid.";
                    message += "\ntried to insert [" + column + ", " + value + "] into " + ToString();
                    throw new System.ArgumentException(message);
                }
                if (column >= 0)
                {
                    clockwiseDistances[column] = value;
                }
                else
                {
                    counterClockwiseDistances[(column + 1) * -1] = value;
                }
            }
        }

        public bool Fits(int height, int clockwiseWidth, int counterClockwiseWidth)
        {
            if (height <= 0)
            {
                throw new System.ArgumentException("the height of a door-offset must be > 0, but it was " + height);
            }
            if (clockwiseWidth < 0)
            {
                throw new System.ArgumentException("the clockwiseWidth of a door-offset must be >= 0, but it was " + clockwiseWidth);
            }
            if (counterClockwiseWidth < 0)
            {
                throw new System.ArgumentException("the counterClockwiseWidth of a door-offset must be >= 0, but it was " + counterClockwiseWidth);
            }

            return this[clockwiseWidth] >= height && this[-1 * counterClockwiseWidth] >= height;
        }

        public override string ToString()
        {
            string result = "";
            for (int i = -1 * MAX_ROOM_SIZE; i <= MAX_ROOM_SIZE; i++)
            {
                result += this[i];
            }
            return result;
        }
    }
    #endregion
}
