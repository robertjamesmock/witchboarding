﻿using static GoodMaths;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[Serializable]
public class RoomMetadata
{
    [Serializable]
    public class DoorOffset {
        /// <summary>
        /// Room height in room segments, from the perspective of an opposing entrance
        /// </summary>
        public int height;

        /// <summary>
        /// Distance in room segments from (the room segment containing this door) to (the room segment at the edge of this room)
        /// in the rightward direction from the perspective of an opposing entrance
        /// </summary>
		public int clockwiseWidth;

        /// <summary>
        /// Distance in room segments from (the room segment containing this door) to (the room segment at the edge of this room)
        /// in the leftward direction from the perspective of an opposing entrance
        /// </summary>
        public int counterClockwiseWidth;

        /// <summary>
        /// The coordinates of the room segment containing this door
        /// </summary>
		public Point segmentIndex;

        /// <summary>
        /// The direction this door is facing
        /// </summary>
		public Direction doorDirection;

        public DoorOffset(int height, int clockwiseWidth, int counterClockwiseWidth, Point segmentIndex, Direction doorDirection)
        {
            if (height <= 0)
            {
                throw new System.ArgumentException("the height of a door-offset must be > 0, but it was " + height);
            }
            if (clockwiseWidth < 0)
            {
                throw new System.ArgumentException("the clockwiseWidth of a door-offset must be >= 0, but it was " + clockwiseWidth);
            }
            if (counterClockwiseWidth < 0)
            {
                throw new System.ArgumentException("the counterClockwiseWidth of a door-offset must be >= 0, but it was " + counterClockwiseWidth);
            }
            this.height = height;
            this.clockwiseWidth = clockwiseWidth;
            this.counterClockwiseWidth = counterClockwiseWidth;
            this.segmentIndex = segmentIndex;
            this.doorDirection = doorDirection;
        }

        public override string ToString()
        {
            string s = "Door offset:";
            s += "\n     Height = " + height;
            s += "\n     Clockwise width = " + clockwiseWidth;
            s += "\n     Counter-clockwise width = " + counterClockwiseWidth;
            s += "\n     Segment Index = " + segmentIndex;
            s += "\n     Direction = " + doorDirection;
            return s;
        }
    }

    // doorOffsets[direction] = a list of door offsets corresponding to doors facing that direction
    public DoorOffsetCollection doorOffsets;
    public int testInt;

    [Serializable]
    public class DoorOffsetCollection
    {
        [SerializeField]
        private ListDoorOffset[] doorOffsets;

        public DoorOffsetCollection()
        {
            doorOffsets = new ListDoorOffset[4];
            for (int i = 0; i < 4; i++)
            {
                doorOffsets[i] = new ListDoorOffset();
            }
        }

        public List<DoorOffset> this[Direction direction]
        {
            get
            {
                return doorOffsets[(int)direction].doorOffsetList;
            }
            set
            { 
                doorOffsets[(int)direction].doorOffsetList = value;
            }
        }
    }

    [Serializable]
    public class ListDoorOffset : IEnumerable<DoorOffset>
    {
        [SerializeField]
        public List<DoorOffset> doorOffsetList;

        public ListDoorOffset()
        {
            doorOffsetList = new List<DoorOffset>();
        }

        public void Add(DoorOffset doorOffset)
        {
            doorOffsetList.Add(doorOffset);
        }

        public IEnumerator<DoorOffset> GetEnumerator()
        {
            return doorOffsetList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return doorOffsetList.GetEnumerator();
        }

        public DoorOffset this[int i]
        {
            get
            {
                return doorOffsetList[i];
            }
            set
            {
                doorOffsetList[i] = value;
            }
        }
    }

    public RoomMetadata()
    {
        doorOffsets = new DoorOffsetCollection();
    }

    public void AddDoorOffset(Entrance entrance)
    {
        if (entrance == null) { throw new System.ArgumentException("entrance cannot be null"); }

        Direction direction = entrance.direction;
        Point roomDimensions = new Point(entrance.myRoom.Width, entrance.myRoom.Height);
        Point entranceCoords = entrance.Coordinates;

        // Math tricks to calculate the height, clockwiseWidth, and counterClockwiseWidth
        int relativeRoomWidth = Mathf.Abs(Point.Dot(direction.Clockwise().GetOffset(), roomDimensions));
        int relativeRoomHeight = Mathf.Abs(Point.Dot(direction.GetOffset(), roomDimensions));

        int clockwiseWidth = (Mod(
            Point.Dot(direction.Clockwise().GetOffset(), 2 * entranceCoords + Point.one),
            2 * relativeRoomWidth) - 1) / 2;
        int counterClockwiseWidth = relativeRoomWidth - clockwiseWidth - 1;
        int height = relativeRoomHeight;

        DoorOffset doorOffset = new DoorOffset(
            height,
            clockwiseWidth,
            counterClockwiseWidth,
            entranceCoords,
            direction);
        //Debug.Log(doorOffset);
        doorOffsets[direction].Add(doorOffset);
    }
}
