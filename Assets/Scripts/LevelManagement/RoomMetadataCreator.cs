﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEditor;
using System.Collections.Generic;

public class RoomMetadataCreator : UnityEditor.AssetModificationProcessor
{
    static string[] OnWillSaveAssets(string[] paths)
    {
        foreach (string path in paths)
            if (IsScene(path))
            {
                var room = Object.FindObjectOfType<Room>();
                if (room != null)
                {
                    var thisRoomMetadata = new RoomMetadata();

                    for (int i = 0; i < room.Width; i++)
                    {
                        for (int j = 0; j < room.Height; j++)
                        {
                            foreach (var entrance in room[i, j])
                            {
                                //Debug.Log("generating metadata for a door facing " + entrance.direction);
                                thisRoomMetadata.AddDoorOffset(entrance);
                            }
                        }
                    }

                    int buildIndex = SceneUtility.GetBuildIndexByScenePath(path);
                    if (buildIndex == -1)
                    {
                        // add the scene to the build settings
                        EditorBuildSettingsScene[] newScenes = new EditorBuildSettingsScene[EditorBuildSettings.scenes.Length + 1];
                        EditorBuildSettings.scenes.CopyTo(newScenes, 0);
                        newScenes[newScenes.Length - 1] = new EditorBuildSettingsScene(path, true);
                        EditorBuildSettings.scenes = newScenes;
                        buildIndex = SceneUtility.GetBuildIndexByScenePath(path);
                        if (buildIndex == -1)
                        {
                            throw new System.Exception("excuse me wHAT");
                        }
                    }

                    var loadableRoomCollectionMetadata = RoomCollectionMetadata.EditorLoad(RoomCollectionMetadata.LOADABLE_ROOM_COLLECTION_ASSET_PATH);
                    var bossRoomCollectionMetadata = RoomCollectionMetadata.EditorLoad(RoomCollectionMetadata.BOSS_ROOM_COLLECTION_ASSET_PATH);

                    bool loadableRcmChanged;
                    bool bossRcmChanged;
                    switch (room.roomType)
                    {
                        case RoomType.Starting:
                            loadableRcmChanged = loadableRoomCollectionMetadata.BuildIndexToRoomMetadata.Remove(buildIndex);
                            bossRcmChanged = bossRoomCollectionMetadata.BuildIndexToRoomMetadata.Remove(buildIndex);
                            break;
                        case RoomType.Loadable:
                            bossRcmChanged = bossRoomCollectionMetadata.BuildIndexToRoomMetadata.Remove(buildIndex);
                            loadableRoomCollectionMetadata.BuildIndexToRoomMetadata[buildIndex] = thisRoomMetadata;
                            loadableRcmChanged = true;
                            break;
                        case RoomType.Boss:
                            loadableRcmChanged = loadableRoomCollectionMetadata.BuildIndexToRoomMetadata.Remove(buildIndex);
                            bossRoomCollectionMetadata.BuildIndexToRoomMetadata[buildIndex] = thisRoomMetadata;
                            bossRcmChanged = true;
                            break;
                        default:
                            throw new System.Exception("illegal room type");
                    }

                    List<string> changedRcmPaths = new List<string>();
                    if (loadableRcmChanged)
                    {
                        loadableRoomCollectionMetadata.Save(RoomCollectionMetadata.LOADABLE_ROOM_COLLECTION_ASSET_PATH);
                        changedRcmPaths.Add(RoomCollectionMetadata.LOADABLE_ROOM_COLLECTION_ASSET_PATH);
                    }
                    if (bossRcmChanged)
                    {
                        bossRoomCollectionMetadata.Save(RoomCollectionMetadata.BOSS_ROOM_COLLECTION_ASSET_PATH);
                        changedRcmPaths.Add(RoomCollectionMetadata.BOSS_ROOM_COLLECTION_ASSET_PATH);
                    }

                    AssetDatabase.ForceReserializeAssets(changedRcmPaths);
                }
            }
        return paths;
    }

    private static bool IsScene(string path)
    {
        return Path.GetExtension(path).Equals(".unity");
    }
}
