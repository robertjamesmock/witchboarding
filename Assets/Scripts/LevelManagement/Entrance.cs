﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entrance : MonoBehaviour {
	public Direction direction;
	public Entrance ConnectedEntrance;
	public Room myRoom;
	public Room.RoomSegment myRoomSegment;

	public Point Coordinates => myRoomSegment.coordinates;

	private void OnTriggerEnter2D(Collider2D collision) {
		Playboarder boarder = collision.gameObject.GetComponent<Playboarder>();
		if (boarder != null) {
			if (ConnectedEntrance != null) {
				ConnectedEntrance.myRoom.RoomEnter();
			}
		}
	}
	public void Close() {

		BoxCollider2D entranceCollider = GetComponent<BoxCollider2D>();
		entranceCollider.isTrigger = false;
		gameObject.layer = LayerMask.NameToLayer("Walls");
		GetComponent<SpriteRenderer>().color = Color.white;
	}

	public void Open() {
		BoxCollider2D entranceCollider = GetComponent<BoxCollider2D>();
		entranceCollider.isTrigger = true;
		gameObject.layer = LayerMask.NameToLayer("Default");
		GetComponent<SpriteRenderer>().color = Color.clear;
	}

	private void Start() {
		RaycastHit2D[] hits = Physics2D.RaycastAll((Vector2)transform.position + direction.GetOffset(), direction.Opposite().GetOffset(), 2f);
		foreach (var hit in hits) {
			Entrance maybeDoor = hit.collider.GetComponent<Entrance>();
			if(maybeDoor!=null && maybeDoor != this) {
				ConnectedEntrance = maybeDoor;
				maybeDoor.ConnectedEntrance = this;
				maybeDoor.Open();
				Open();
				break;
			}
		}
	}
}
