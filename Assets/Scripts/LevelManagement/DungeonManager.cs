﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum DungeonType
{
    Small,
    Large
}

public class DungeonManager : PersistentSingleton<DungeonManager>
{
    public int Level {
        get => info.level;
        private set { info.level = value; } }
    public Point NegativeBounds {
        get => info.negativeBounds;
        private set { info.negativeBounds = value; } }
    public Point PositiveBounds {
        get => info.positiveBounds;
        private set { info.positiveBounds = value; } }
    public int MaxRooms {
        get => info.maxRooms;
        private set { info.maxRooms = value; } }
    public string ThemeOrBossOrSomethingIdfk {
        get => info.themeOrBossOrSomethingIdfk;
        private set { info.themeOrBossOrSomethingIdfk = value; } }

    private DungeonInfo info;

    private static readonly DungeonInfo BIG_DUNGEON = new DungeonInfo(
        type: DungeonType.Large,
        level: 1,
        negativeBounds: new Point(0, -10),
        positiveBounds: new Point(int.MaxValue / 2, 10),
        maxRooms: 20,
        themeOrBossOrSomethingIdfk: null,
        startingRoom: "StartingRoom-3x2-r"
    );

    private static readonly DungeonInfo SMALL_DUNGEON = new DungeonInfo(
        type: DungeonType.Small,
        level: 1,
        negativeBounds: new Point(0, -3),
        positiveBounds: new Point(int.MaxValue / 2, 3),
        maxRooms: 3,
        themeOrBossOrSomethingIdfk: null,
        startingRoom: "StartingRoom-3x2-r"
    );

    protected override void InitSingleton()
    {
        base.InitSingleton();
        info = new DungeonInfo();
    }

    /// <summary>
    /// Sets the dungeon settings according to the passed dungeon type
    /// </summary>
    public void Set(DungeonType type)
    {
        switch (type)
        {
            case (DungeonType.Large):
                info = new DungeonInfo(BIG_DUNGEON);
                break;
            case (DungeonType.Small):
                info = new DungeonInfo(SMALL_DUNGEON);
                break;
            default:
                throw new Exception("invalid dungeon type");
        };
    }

    /// <summary>
    /// Resets the dungeon to the starting settings
    /// </summary>
    public void Reset()
    {
        Set(info.type);
    }

    /// <summary>
    /// Increases the positive bounds, negative bounds, and max rooms in the dungeon settings
    /// </summary>
    public void IncreaseDungeonSize()
    {
        NegativeBounds += Direction.down.GetOffset();
        PositiveBounds += Direction.up.GetOffset();
        MaxRooms = Mathf.RoundToInt(MaxRooms * 1.4f);
        Level++;
    }

    /// <summary>
    /// Loads a new level with the current dungeon settings
    /// </summary>
    public void LoadDungeon()
    {
        SceneManager.LoadScene(info.startingRoom);
    }

#if UNITY_EDITOR
    private void OnGUI() {
		GUI.Label(new Rect(0, 0, 200, 200), "dungeon dimentsions:\n" + NegativeBounds.ToString() + " to " + PositiveBounds.ToString() + "\nmax rooms: " + MaxRooms.ToString());
	}
#endif

    private class DungeonInfo
    {
        public DungeonType type;
        public int level;
        public Point negativeBounds;
        public Point positiveBounds;
        public int maxRooms;
        public string themeOrBossOrSomethingIdfk;
        public string startingRoom;

        public DungeonInfo() { }

        public DungeonInfo(DungeonInfo original) : this(
            original.type,
            original.level,
            original.negativeBounds,
            original.positiveBounds,
            original.maxRooms,
            original.themeOrBossOrSomethingIdfk,
            original.startingRoom)
        { }

        public DungeonInfo(DungeonType type, int level, Point negativeBounds, Point positiveBounds, int maxRooms, string themeOrBossOrSomethingIdfk, string startingRoom)
        {
            this.type = type;
            this.level = level;
            this.negativeBounds = negativeBounds;
            this.positiveBounds = positiveBounds;
            this.maxRooms = maxRooms;
            this.themeOrBossOrSomethingIdfk = themeOrBossOrSomethingIdfk;
            this.startingRoom = startingRoom;
        }
    }
}
