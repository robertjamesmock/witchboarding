﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Room))]
[CanEditMultipleObjects]
public class RoomEditor : Editor
{
	SerializedProperty segmentColumns;
	SerializedProperty triggerers;
    //SerializedProperty isStartingRoom;
    SerializedProperty roomType;

	private void OnEnable() {
		segmentColumns = serializedObject.FindProperty("roomSegmentColumns");
		triggerers = serializedObject.FindProperty("triggerers");
        //isStartingRoom = serializedObject.FindProperty("startingRoom");
        roomType = serializedObject.FindProperty("roomType");
	}

	public override void OnInspectorGUI() {

		serializedObject.Update();
        foreach (object obj in serializedObject.targetObjects)
        {
            ((Room)obj).EnsureMinimumGrid();
            serializedObject.Update();
        }

		

		int width = EditorGUILayout.IntSlider("width", segmentColumns.arraySize, 1, RoomManager.MAX_ROOM_SIZE);

		int height = EditorGUILayout.IntSlider("height", segmentColumns.GetArrayElementAtIndex(0).FindPropertyRelative("segments").arraySize, 1, RoomManager.MAX_ROOM_SIZE);

		foreach (object obj in serializedObject.targetObjects) {
			((Room)obj).EnsureGrid(width, height);
			serializedObject.Update();
		}

		EditorGUILayout.BeginVertical();
		{//unnecessary scoping, doing this to make tabbing make more sense
			EditorGUILayout.BeginHorizontal();
			{
				EditorGUILayout.LabelField(" ", GUILayout.Width(16));
				int y = height - 1;
				for (int x = 0; x < width; x++) {
					SerializedProperty segmentProperty = GetSegmentProperty(x, y);
					SerializedProperty doorProperty = segmentProperty.FindPropertyRelative("UpDog");
					bool hasDoor = doorProperty.objectReferenceValue != null;
					bool wantsDoor = EditorGUILayout.Toggle(hasDoor, GUILayout.Width(16));
					EnsureDoor(x, y, hasDoor, wantsDoor, Direction.up);
				}

				EditorGUILayout.LabelField(" ", GUILayout.Width(16));
			}
			EditorGUILayout.EndHorizontal();
			for (int y = height-1; y >= 0; y--) {
				EditorGUILayout.BeginHorizontal();
				{
					{
						int x = 0;
						bool hasDoor = GetSegmentProperty(x, y).FindPropertyRelative("LeftEntrance").objectReferenceValue != null;
						bool wantsDoor = EditorGUILayout.Toggle(hasDoor, GUILayout.Width(16));
						EnsureDoor(x, y, hasDoor, wantsDoor, Direction.left);
					}
					for (int x = 0; x < width; x++) {
						EditorGUILayout.LabelField(" ", GUILayout.Width(16));
					}
					{
						int x = width - 1;
						bool hasDoor = GetSegmentProperty(x, y).FindPropertyRelative("RightDoor").objectReferenceValue != null;
						bool wantsDoor = EditorGUILayout.Toggle(hasDoor, GUILayout.Width(16));
						EnsureDoor(x, y, hasDoor, wantsDoor, Direction.right);
					}
				}
				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.BeginHorizontal();
			{
				EditorGUILayout.LabelField(" ", GUILayout.Width(16));
				int y = 0;
				for (int x = 0; x < width; x++) {
					bool hasDoor = GetSegmentProperty(x, y).FindPropertyRelative("DownPortal").objectReferenceValue != null;
					bool wantsDoor = EditorGUILayout.Toggle(hasDoor, GUILayout.Width(16));
					EnsureDoor(x, y, hasDoor, wantsDoor, Direction.down);
				}

				EditorGUILayout.LabelField(" ", GUILayout.Width(16));
			}
			EditorGUILayout.EndHorizontal();
		}
		EditorGUILayout.EndVertical();

		//isStartingRoom.boolValue = EditorGUILayout.Toggle("starting room?", isStartingRoom.boolValue, new GUILayoutOption[0]);

        roomType.enumValueIndex = (int)(RoomType)EditorGUILayout.EnumPopup("Room type: ", (RoomType)roomType.enumValueIndex);

		foreach (object obj in serializedObject.targetObjects) {
			((Room)obj).CollectTriggerers();
		}
		EditorGUILayout.LabelField(triggerers.arraySize+" triggerers");
		serializedObject.ApplyModifiedProperties();
	}

	private void EnsureDoor(int x, int y, bool hasDoor, bool wantsDoor, Direction direction) {
		if (hasDoor && !wantsDoor) {
            Undo.RecordObject(target, "Removed Door");
			foreach (object obj in serializedObject.targetObjects) {
				((Room)obj).RemoveDoor(x, y, direction);
			}
		} else if (!hasDoor && wantsDoor) {
            Undo.RecordObject(target, "Added Door");
            foreach (object obj in serializedObject.targetObjects) {
				((Room)obj).MakeDoor(x, y, direction);
			}
		}
	}

	SerializedProperty GetSegmentProperty(int x, int y) {
		return segmentColumns.GetArrayElementAtIndex(x).FindPropertyRelative("segments").GetArrayElementAtIndex(y);
	}

}