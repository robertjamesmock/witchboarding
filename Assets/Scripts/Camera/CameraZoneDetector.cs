﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoneDetector : MonoBehaviour {
    private cameraFollow cameraFollow;
    private Collider2D currentCameraZone;
    private HashSet<Collider2D> currentCollisions;
    private CapsuleCollider2D myCollider;

    private void Awake()
    {
        currentCollisions = new HashSet<Collider2D>();
        myCollider = GetComponent<CapsuleCollider2D>();
        cameraFollow = Camera.main.GetComponent<cameraFollow>();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        CameraBoundingBox box = collision.GetComponent<CameraBoundingBox>();

        if (box != null)
        {
            currentCollisions.Add(collision);
            currentCameraZone = collision;
            cameraFollow.ChangeBoundingBox(box);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        CameraBoundingBox box = collision.GetComponent<CameraBoundingBox>();

        if (box == null)
        {
            return;
        }

        currentCollisions.Remove(collision);

        if (!myCollider.IsTouching(currentCameraZone))
        {
            // player exited the current camera zone before exiting the previous one,
            // therefore missing the OnTriggerEnter of the previous camera zone,

            if (currentCollisions.Count > 0)
            {
                // still in at least one camera zone, so set camera bounding box to one of them
                foreach (Collider2D zone in currentCollisions)
                {
                    currentCameraZone = zone;
                    cameraFollow.ChangeBoundingBox(zone.GetComponent<CameraBoundingBox>());
                    break;
                }
            }
            else
            {
                // not in a camera zone, so set camera to no camera zone
                currentCameraZone = null;
                cameraFollow.SetNoBoundingBox();
            }

        }

    }
}
