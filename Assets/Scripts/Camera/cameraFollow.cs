﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {
    public Transform ToFollow;
    public Vector3 Offset = new Vector3(0, 2, -10);

    public float CameraSnappiness = 0.25f;  // time in s it takes for the camera to catch up ~90% of the way to rabbz
    public float TransitionTime = 1;
    public float DefaultCameraSize = 5;

    private readonly Rect NoBoundingBox = new Rect(float.MinValue / 2, float.MinValue / 2, float.MaxValue, float.MaxValue);
    private Camera cam;
    private Rect nextBoundingBox;
    //private Rect prevBoundingBox;
    private Vector3 prevBoundingBoxPosition;
    private float nextCameraSize;
    private float prevCameraSize;
    private float transitionTimeRemaining;

    private void Awake()
    {
        transitionTimeRemaining = 0;
        cam = GetComponent<Camera>();
        nextBoundingBox = NoBoundingBox;
        nextCameraSize = DefaultCameraSize;
    }

    void Update() {
        Vector3 pos = ToFollow.position + Offset; // track player

        Vector3 camCurrentPosition = cam.transform.position;
        Vector3 camDesiredPosition;

        if (transitionTimeRemaining > 0)
        {
            // transitioning to a new bounding box
            float prevWeight = transitionTimeRemaining / TransitionTime;
            float nextWeight = 1 - prevWeight;

            // math tricks to make transition feel more smooth
            prevWeight = Mathf.Pow(prevWeight, 2);
            nextWeight = Mathf.Pow(nextWeight, 2);

            // interpolate position between previous camera position and position in next bounding box
            camDesiredPosition =
                (prevBoundingBoxPosition * prevWeight
                    + PositionInBoundingBox(pos, nextBoundingBox) * nextWeight)
                / (prevWeight + nextWeight);

            // interpolate size between prev and next bounding box
            cam.orthographicSize = (prevCameraSize * prevWeight + nextCameraSize * nextWeight) / (prevWeight + nextWeight);

            transitionTimeRemaining -= Time.deltaTime;
            if (transitionTimeRemaining < 0) transitionTimeRemaining = 0;
        }
        else
        {
            camDesiredPosition = PositionInBoundingBox(pos, nextBoundingBox);
        }

        Vector3 camCurrentToDesiredPosition = camDesiredPosition - camCurrentPosition;
        // move camera toward desired position, but not past
        cam.transform.position = camCurrentPosition + camCurrentToDesiredPosition * Mathf.Min(1, Time.deltaTime * (2 / CameraSnappiness));
    }

    public void ChangeBoundingBox(CameraBoundingBox box)
    {
        //prevBoundingBox = nextBoundingBox;
        prevBoundingBoxPosition = cam.transform.position;
        prevCameraSize = cam.orthographicSize;
        nextBoundingBox = box.AsRect;

        float boxAspectRatio = nextBoundingBox.width / nextBoundingBox.height;
        float screenAspectRatio = ScreenAspectRatio();

        if(boxAspectRatio > screenAspectRatio)
        {
            // box is wider than screen so make height of camera = height of box
            nextCameraSize = nextBoundingBox.height / 2;
        } else
        {
            // box is taller than screen so make width of camera = width of box
            nextCameraSize = nextBoundingBox.width / (2 * screenAspectRatio);
        }

        transitionTimeRemaining = TransitionTime;
    }

    public void SetNoBoundingBox()
    {
        //prevBoundingBox = nextBoundingBox;
        prevBoundingBoxPosition = cam.transform.position;
        prevCameraSize = cam.orthographicSize;
        nextBoundingBox = NoBoundingBox;
        nextCameraSize = DefaultCameraSize;
        transitionTimeRemaining = TransitionTime;
    }

    private Vector3 PositionInBoundingBox(Vector3 cameraPos, Rect boundingBox)
    {
        float cameraCenterToEdgeY = cam.orthographicSize;
        float cameraCenterToEdgeX = cameraCenterToEdgeY * ScreenAspectRatio();

        return new Vector3(
            Mathf.Max(Mathf.Min(cameraPos.x, boundingBox.xMax - cameraCenterToEdgeX), boundingBox.xMin + cameraCenterToEdgeX),
            Mathf.Max(Mathf.Min(cameraPos.y, boundingBox.yMax - cameraCenterToEdgeY), boundingBox.yMin + cameraCenterToEdgeY),
            cameraPos.z);
    }

    private float ScreenAspectRatio()
    {
        return Screen.width / (float)Screen.height;
    }
}