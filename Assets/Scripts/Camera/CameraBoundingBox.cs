﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBoundingBox : MonoBehaviour {

    public Rect AsRect
    {
        get
        {
            return new Rect(
                transform.position - transform.localScale * 0.5f,
                transform.localScale);
        }
    }
}
